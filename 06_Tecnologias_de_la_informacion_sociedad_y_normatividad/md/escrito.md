# Tecnologías de la información, sociedad y normatividad

\vskip 3em

\noindent Cuando se habla del desarrollo tecnológico es una concepción 
muy extendida que esta toma en cuenta el «progreso» de las ciencias, 
la tecnología, la ingeniería y las matemáticas ([CTIM]{.versalita}). 
Del mismo modo, en el discurso en pos de los avances tecnológicos en 
varias ocasiones se menciona su pertinencia social. Sin embargo, rara 
vez se cae en cuenta que este discurso no solo versa sobre cómo 
efectivamente la sociedad se comporta y asimila el desarrollo 
tecnológico, sino también en torno a cómo la sociedad *ha de* 
adaptarse a las tecnologías de la información y la comunicación o 
sobre cómo estas tecnologías *deberían* aplicarse para el beneficio 
social así como cuáles son las medidas por las que estas *tienen que* 
adaptarse a las posibilidades y límites de cada comunidad.

Lo que antes se pensaba como un discurso con especial enfoque en las
ciencias y la tecnología o por lo menos como un discurso social 
«neutro» u «optimista», ahora permite hacer patente su común sentido
normativo. Para ello, a partir de algunas lecturas de Wiener, Wells,
Bush, Engelbart, Licklider y Nelson se prentenderá demostrar esta
hipótesis al mismo tiempo que se busca evidenciar que no es ninguna
novedad en el discurso de las tecnologías de la información, sino
más bien una base que permite a diversos teóricos ir más allá del 
estado actual de la sociedad y de la técnica, pero un poco más acá de
la ciencia ficción.

\vskip 1em\noindent En la década de los cuarenta Norbert Wiener, 
matemático estadunidense, acuñó el término «cibernética». La primera
acepción del concepto se definió como «el estudio teórico de los
procesos de comunicación y de control en sistemas biológicos, 
mecánicos y artificiales» --parencite(siles2007)--. Una de las
propuestas más llamativas de Wiener fue el hacer énfasis en la 
información como «el contenido de lo que es objeto de intercambio con
el mundo externo, mientras nos ajustamos a él y hacemos que se acomode
a nosotros» --parencite(siles2007)--.

Esta definición de información permite entender a cualquier fenómeno,
y no solo los de la comunicación —o al menos esa es la pretensión de
Wiener— y las relaciones entre humanos o entre estos y el resto del
en torno como fundamentalmente un intercambio de información
--parencite(siles2007)--. Y aunque en un primer momento este estudio
puede entenderse como primordialmente descriptivo; es decir, sobre
cómo es que *se dan* estos fenómenos, el mismo término de 
«cibernética» pretende más bien modificar o intervenir en las 
distintas relaciones entabladas en los sistemas.

La palabra «cibernética» viene de la voz griega que en inglés 
significa «*steersman*» --parencite(wiener1948)--, «timonero» en 
español: la persona que *tiene el control* del timón. Para Wiener es
manifiesto que la información y la comunicación son elementos que
van más allá del individuo y se anclan más bien a fenómenos sociales o
de comunidades --parencite(wiener1948)--. Además, lo que el avance
científico durante la Segunda Guerra Mundial demostró es la 
posibilidad de un sin fin de posibilidades en el uso de la información
y de las formas de comunicación que puede ser tanto para bien como
para mal --parencite(wiener1948)--.

En un sentido negativo, el desarrollo tecnológico puede desplazar la
actividad humana hasta el punto donde no valga la pena emplear su
trabajo --parencite(wiener1948)--. Sin embargo, esto implica una
reducción del valores a las nociones de «compra» y «venta», donde
la competitividad es la clave: solo las personas cualificadas o
especializadas pueden sobrevivir en esta clase de «progreso»
--parencite(wiener1948)--. En un sentido positivo, la tecnología puede
ser un auxiliar para disminuir el esfuerzo humano y planificar de
una mejor manera el desarrollo social --parencite(wiener1948)--. 

Ante estas posibilidades, los fundadores de la cibernética no se 
encuentran en una posición moral cómoda, ya que están al tanto de 
las posibilidades y peligros de la tecnología pero sin contar con una 
gran capacidad para detener el desarrollo tecnológico; incluso este 
deseo solo desembocaría en permitir que este avance se dé en manos de 
personas más irresponsables --parencite(wiener1948)--. La única 
alternativa para el campo de la cibernética es afrontar esta situación
y tratar de orientar el desarrollo tecnológico lo más alejado de la
guerra o la explotación, donde esta misma disciplina puede permitir
un mejor entendimiento del hombre y la sociedad 
--parencite(wiener1948)--.

Esta *obligación moral* no es fortuita ni una figura retórica dentro
del discurso en pos del desarrollo tecnológico. Para Wiener son pocas
las personas con la preparación interdisciplinaria suficiente como
para tratar y entender los problemas planteados por la cibernética
--parencite(wiener1948)--. Así como en una embarcación solo un 
compacto conjunto de personas tienen los conocimientos —y la 
autoridad— para poder tomar el timón y así mantener o virar el curso;
en nuestras comunidades solo unas cuantas personas tienen la capacidad
para comprender la problemática de la cibernética y así *evitar* que
el curso sea orientado a la guerra o a la explotación.

El carácter elitista o el optimismo ante el avance de las 
[CTIM]{.versalita} son elementos que se presentan en otros tipos de
discursos. No obstante, en el surgimiento de la cibernética es 
perceptible una característica del modelo discursivo bajo análisis:
el desarrollo tecnológico es imparable, pero su impacto social es
manejable.

El discurso en pos de la tecnología jamás atenta a su propia 
posibilidad de crecimiento o reproducción, en su lugar esto se 
presenta a manera de axioma desde donde han de erigirse una serie de
premisas que den pautas a normas para su desarrollo. Y aunque parezca
irrelevante, con semejante mecanismo es como este modelo discursivo
puede eyectar toda clase de discursos que no compartan este axioma y
postularlos como ejemplos de la «mala» aplicación del desarrollo 
tecnológico —mas no el mismo desarrollo— o la ceguera ante algo tan
evidente desde los inicios de la modernidad: el «progreso» —sin 
mención expresa del origen y justificación de la historia, sino es que
la historia de Europa, entendida como progreso humano—.

\vskip 1em\noindent Aunque la reducción de los discursos en contra
del desarrollo tecnológico en sí a «malas» aplicaciones o a la
«carencia» de perspectiva histórica suene a simplificación, en H. G.
Wells se percibe las posibilidades hacia donde es posible dirigir a
este modelo discursivo.

En su charla «The Brain Organization of the Modern World» no solo 
entre ve la posibilidad de la «unificación mental» del mundo por medio
del avance tecnológico --parencite(wells2013)-- sino también la 
conjunción entre el desarrollo de la tecnología y el desarrollo de
personas cada vez más informadas --parencite(wells2013)--.

La propuesta inicial es la búsqueda de un mecanismo tecnológico que
permita la creación de una enciclopedia mundial. Pero no solo eso,
sino un tipo de obra que, por su carácter global, no puede estar 
abierta a dogmas aunque sí a criticismo --parencite(wells2013)--. La 
idea central es que la humanidad podrá progresar si todos sus miembros 
están «bien» informados, ya que de este modo los mismos individuos 
tendrán la capacidad de discernir entre lo verdadero y lo falso, o lo 
«bueno» y lo «malo».

No es novedad que ciertos los modelos discursivos estén basados en 
ideales tomados por universales, aunque con origen específico en la 
modernidad Europea, como son: el laicismo como abolición de dogmas, la 
libertad como destrucción de sistemas opresivos, la voluntad del 
individuo como expresión de la personalidad o la educación y las 
ciencias como medios de emancipación. Lo que es característico del 
modelo discursivo en pos de la tecnología es que estos ideales se 
ajustan sin problemas en contra de una de las concepciones más 
constantes en la modernidad Europea: la dicotomía entre el cuerpo y la 
mente.

A diferencia de los discursos desarrollistas o progresistas, el modelo
discursivo en pos del desarrollo tecnológico no solo enfoca el auge de
las máquinas a sus consecuencias en la pertinencia industrial, 
económica o social. Además de ello, la máquina no se concibe ya como 
un agente ajeno a la persona, sino como un punto de enlace que permite 
difuminar la dicotomía entre el cuerpo y la mente. La máquina deja de 
ser un conjunto de partes mecánicas que auxilia al hombre —incluso 
hasta reemplazarlo— a pasar a ser un objeto sofisticado que ayuda a 
superar uno de los grandes problemas frutos de la modernidad: el 
cuerpo como objeto de carne y hueso y la mente como sinónimo de la 
subjetividad etérea del individuo. La máquina ya no es objeto, porque 
abandona sus cualidades físicas a favor de los intereses metafísicos 
de sujetos que ya no quieren sentirse escindidos. La máquina deviene 
en sujeto o al menos en una parte crítica de los sujetos…

\vskip 1em\noindent La máquina como elemento fundacional de un sujeto
con capacidad de discernir entre lo correcto y lo incorrecto también
se encuentra presente en el trabajo de Vannevar Bush.

Una de las premisas de Bush es que el estado actual de la 
investigación acarrea que cualquier disciplina contenga ya una gran
cantidad de información cuyo manejo y análisis por una persona ya
resulta inviable --parencite(bush1945)--. Como solución a este 
problema es como surge la idea del «memex», un dispositivo que permite
a la persona almacenar la información, pero no solo eso, sino que
gracias a un modelo asociativo es posible consultar esta información
de manera flexible y a gran velocidad --parencite(bush1945)--.

El memex lo asocia Bush a un tipo de enciclopedia 
--parencite(bush1945)--, pero por el mismo motivo quizá es pertinente
evitar pensar a este dispositivo como un compendio externo de 
información. Más que un libro o una plataforma digital en la que el
sujeto puede consultar información, el memex es un dispositivo muy
cercano a las funciones realizadas por la memoria. 

El término «memex» es un acrónimo de ***mem**ory ind**ex*** o 
***mem**ory **ex**tender*. Esto significa que la intención del memex
no es la sustitución de lo que hemos estado entendiendo como medios
de almacenamiento de la información, como las enciclopedias o las
bases de datos, sino como un mecanismo auxiliar a la memoria. Este
dispositivo implica la necesidad de contener información pero su
característica más notable no es el almacenamiento sino los procesos
asociativos que ayudan a generar nueva información. El memex, más que
una «máquina», es la metáfora de cómo Bush supone que funciona la
mente humana: el memex es un dispositivo concreto que requiere el
sujeto para poder estar a la altura del mar de información en el que
ahora se encuentra. El memex es el eslabón para la evolución de la
mente humana, en lugar de ser un objeto abstracto de almacenamiento
de la información.

Con esto se hace posible ser más específico con la postura que tiene
el modelo del discurso bajo análisis respecto a las máquinas: estas
pasan a ser dispositivos que «aumentan» varias funciones preciadas
por los sujetos como son la memoria y la capacidad de generar nuevos
conocimientos mediante asociación. Y aunque es muy debatible hasta que
grado estos dispositivos no son objetos insertos en los sujetos a
modo de prótesis, la misma carencia de especificidad técnica o los
mismos yerros en la práctica científica parecen afectar poco la
convicción de que es posible emular o amplificar la mente humana: el
punto central es cómo el desarrollo tecnológico hace posible ir más
allá de las condiciones biológicas dadas a los sujetos.

\vskip 1em\noindent En caso de Douglas Engelbart en este sentido no
difiere mucho al de Bush. Para Engelbart también la complejidad de
los problemas ha superado la capacidad ordinaria del hombre para poder
solucionarlos --parencite(engelbart1962)--. Por este motivo es 
necesario aumentar el intelecto humano mediante mecanismos 
tecnológicos que le permitan una mayor capacidad de manejo de la
información con el objetivo de gestar nuevas técnicas, procedimientos
y sistemas para solucionar diversas necesidades sociales
--parencite(engelbart1962)--.

A diferencia de Bush, en Engelbart se observa un nexo directo entre
la necesidad —que no ya mera posibilidad— del aumento de las funciones
cognitivas del sujeto con la relevancia social que esto tiene. En
este sentido las normas ya no son solo el sobrentendido entre lo que
el sujeto en sí mismo entiende por «bien» y «mal», sino también
la creación de «reglas de juego» que permitan el desarrollo deseable
para este nuevo tipo de cognitividad.

Estas reglas es lo que Engelbart desarrolla y llama «marco 
conceptual», cuyo énfasis práctico es evidente al catalogarlo también
como un «plan de acción» --parencite(engelbart1962)--. Esto permite
resaltar otra característica del modelo discursivo en pos del
desarrollo tecnológico: aunque en la práctica el discurso se presta a 
múltiples interpretaciones, cuenta con fuertes intenciones 
pragmáticas. Este modelo discursivo no pretende interpretar al mundo
y a las comunidades, sino que busca transformarlos. Pero este cambio 
no está ligado directamente a los procesos de producción, sino a los 
mecanismos que permiten la creación de nuevos conocimientos o a la 
solución de problemas.

No está de sobra indicar que la transformación tiene también fuertes
connotaciones éticas donde el punto de mira no es la transformación
por la transformación misma, sino una vía encaminada al bien social.
Por ello, hay cambios pertinentes y otros catalogados como indeseables
e incluso peligrosos para todo el género humano.

\vskip 1em\noindent El plan de acción, a diferencia de otros modos
de actuar, ya no pretende depender de la situación geográfica de
los sujetos. En su lugar, J. C. R. Licklider busca una simbiosis que 
permita un modelo cooperativo de comunicación basada en el consenso
--parencite(licklider1960)--.

Licklider define la simbiosis como «una vivencia en conjunto con una
íntima asociación, o incluso algo más cercano, de dos organismos 
completamente diferentes» --parencite(licklider1960)--. Y al mismo 
tiempo el término «organismo» ya no solo comprende entes orgánicos,
sino también dispositivos electrónicos en conjunción con el ser
humano: el hombre-máquina y, siendo más específicos, el 
hombre-computadora --parencite(licklider1960)--.

Esto implica la posibilidad de una integración en donde ya no es
discernible lo orgánico de lo maquinal y que al mismo tiempo permite
una interacción con la información ya no solo como un paradigma
comunicativo donde se envía o recibe información 
--parencite(licklider1960)--. Este otro modo de interacción produce
una intervención continúa de la información que hará de los procesos
comunicativos algo más fuerte, claro y disfrutable 
--parencite(licklider1960)--.

Aquí se desprenden al menos otras dos características del modelo
discursivo bajo análisis. Por un lado, ya no solo existe una difusión
de la dicotomía del cuerpo y el alma, sino también de lo orgánico y
lo maquinal —lo «natural» y lo «artificial»—, que beneficia a la
sociedad. Por otro, el discurso tiene una fuerte pretensión global,
hasta el grado donde la geografía no es una limitante ni algo
esencial para la comunicación.

El concepto de naturaleza y de mundo sufre una «mutación» ontológica
al difuminar dos dicotomías centrales para los conceptos modernos de 
hombre y mundo: ya no hay barreras que dividan al sujeto de manera
interna ni externa. El sujeto se vuelve carta intercambiable entre
su mente y su cuerpo, pero también entre su medio y su individualidad.

El sujeto es tan homologado que no hay barrera geográfica que lo
limite, haciendo que sus procesos comunicativos se centren más en el
interés que tenga este sujeto de intercambiar información con otros.
El interés individual o social termina por desbordar las distintas
geografías pero también políticas globales…

\vskip 1em\noindent Cuando parecía que el modelo discursivo en pos
del desarrollo tecnológico no podía ir más lejos en cuanto a la 
difuminación de dicotomías, aparece Ted Nelson.

Para Nelson es necesario cerrar la brecha entre lo público y lo
privado --parencite(nelson1974)--. En el caso de las tecnologías de la
información esta norma ha de aplicarse para evitar la brecha que 
separa a los expertos de los neófitos: el desarrollo tecnológico 
debería ser de un orden público --parencite(nelson1974)--. La manera
en como es posible evitar que los expertos acaparen —y controlen—
aspectos de la realidad es haciendo que el público general forme
parte activa de las nuevas arquitecturas de la comunicación que están
cambiando rasgos fundamentales de nuestra sociedad 
--parencite(nelson1974)--. La realidad tecnológica es fruto y límite
de los sueños que se han vuelto dominantes en la actual gestación
social, solo si las personas se vuelven hacedores de sus propios
sueños es como puede evitarse un desarrollo tecnológico que solo
beneficie a unos cuantos --parencite(nelson1974)--.

La necesidad de hacer del discurso en pos del desarrollo tecnológico
un asunto de discusión pública permite rescatar el carácter histórico
de la tecnología. A diferencia de los discursos desarrollistas o
progresistas, este modelo discursivo no ve al desarrollo tecnológico
desanclado del contexto histórico de las sociedades, o al menos eso
intenta. En su lugar, percibe al presente tecnológico como originado
intrínsecamente de los hechos pasados: el humano en su obrar ha 
estipulado de antemano la dirección que tomará su desarrollo 
tecnológico. Por ello rescata la importancia de tomar como asunto de
interés público temas que se consideran tan técnicos e incluso tan
alejados de las esferas de influencia social del presente: no es
solo el presente lo que está en juego, sino las posibilidades futuras
que se tendrá como sujetos o comunidades —véase como ejemplos actuales
los problemas relativos a la privacidad de los usuarios o de los 
alcances de la inteligencia artificial desarrollada por Estados o
empresas privadas—.

\vskip 1em\noindent Para terminar, con estos autores ya es posible
constatar una serie de características que conforman el modelo
discursivo a favor del desarrollo tecnológico:

* La posibilidad del desarrollo tecnológico no está en entredicho.
* El desarrollo tecnológico es incontenible, pero manejable en pos
  de un beneficio social.
* El desarrollo tecnológico implica la constitución de normas para
  evitar aplicaciones en detrimento de los individuos.
* El desarrollo tecnológico no es un simple aumento de nuestras
  capacidades de producción, de trabajo o de riqueza.
* La máquina no es un artefacto externo, sino un dispositivo interno.
* La máquina no es objeto, sino al menos parte del sujeto.
* La máquina es el punto de partida que permite difuminar la dicotomía
  entre cuerpo y mente.
* La máquina viene a amplificar funciones específicas de la mente de
  un sujeto.
* La máquina permite ir más allá de las condiciones biológicas dadas 
  de los sujetos.
* El discurso es pretensión de acción.
* El discurso se vuelca al bien social, por lo que la constitución de
  normas se vuelve necesario para evitar tergiversaciones no solo
  en el desarrollo tecnológico, sino también del mismo discurso.
* El discurso pretende difuminar tres grandes dicotomías modernas: la 
  del cuerpo y la mente, la de lo público y lo privado, y la de lo 
  natural y lo artificial.
* El discurso se vuelca más a los intereses subjetivos o 
  intersubjetivos que a las limitaciones geográficas o políticas de
  los individuos.
* El presente tecnológico se percibe como fruto de las decisiones que
  como sociedad se dieron en el pasado; el futuro es la oportunidad
  de que más individuos puedan formar parte de esta decisión.

\noindent En este modelo existe un complejo pero también anticipado
entrelazamiento entre el desarrollo tecnológico y el beneficio social,
así como la relación entre el sujeto con los dispositivos 
tecnológicos. Esta vía anticipada relaciona a cada uno de los 
elementos no solo con la expectativa de darle continuidad al mismo
discurso o el modo en como se *ha de desarrollar* la tecnología o
*ha de progresar* la sociedad, sino también al obedecimiento de normas 
implícitas donde el desarrollo tecnológico tiene un campo de 
posibilidades, pero también de limitantes.

¿Cuáles son esos límites autoimpuestos? La delimitación que evita
el desbordamiento del modelo discursivo a distopías, anarquismos
primitivistas, anacronismos, ciencia ficción pero, principalmente, 
para salvaguardar su discurso de sus «malas» aplicaciones o sus
propias simplificaciones. Aunque existen esfuerzos fundamentales para
mantener vivo este modelo a través del avance de las 
[CTIM]{.versalita}, también es cierto que este *élan vital* subsiste
gracias a las distintas metáforas, convicciones y deseos que arropan
al discurso mismo.

---

\pagebreak
\begin{flushleft}
Autor: Ramiro Santa Ana Anguiano. \\ Redactado: junio del 2018. \\ Última modificación: junio del 2018. \\ Escrito bajo \href{https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre}{Licencia Editorial Abierta y Libre}. \\ Contenido disponible en \href{http://xxx.cliteratu.re/}{xxx.cliteratu.re}.
\end{flushleft}
