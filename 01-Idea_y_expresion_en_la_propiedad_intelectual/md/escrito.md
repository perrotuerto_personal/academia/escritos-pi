# Idea y expresión en la propiedad intelectual

\noindent A partir de la segunda mitad del siglo [XX]{.versalita} la propiedad intelectual
([PI]{.versalita}) ha adquirido relevancia económica a través de sus distintas 
manifestaciones, tales como las patentes, las marcas, los derechos de autor, los secretos 
comerciales, los diseños industriales o las indicaciones geográficas, entre otras --parencite(moore2014a)--.
Según la Organización Mundial de la Propiedad Intelectual ([OMPI]{.versalita}),
organismo de la [ONU]{.versalita} encargado de desarrollar un sistema internacional
de [PI]{.versalita}, el crecimiento económico se basa en innovaciones revolucionarias 
que transforman la actividad productiva y dan nacimiento a nuevas industrias, en 
donde la [PI]{.versalita} favorece esta actividad innovadora --parencite(ompi2015a)--.

Si bien esto es una justificación económica y política del sistema de [PI]{.versalita}, 
poco explica el concepto de «propiedad intelectual», ya que este presenta al menos 
tres dimensiones --parencite(shiffrin2007a)--:

1. La [PI]{.versalita} como objeto creado por un sujeto.
2. La [PI]{.versalita} como derecho por el cual se protege al objeto creado.
3. La [PI]{.versalita} como sistema por el cual se aglutinan las distintas manifestaciones 
   de los derechos de [PI]{.versalita}.

\noindent La [PI]{.versalita} como agente favorecedor de la innovación busca justificar la
implementación o la continuidad de diversos derechos de [PI]{.versalita} (segunda
dimensión) en un sistema de intercambio de conocimientos (tercera dimensión) que,
acorde a la [OMPI]{.versalita}, es un motor del crecimiento económico --parencite(ompi2015a)--.
Esto evidencia una deficiencia en el discurso ya que, como diversos investigadores
o defensores de la [PI]{.versalita} han aceptado, *aún no existe una teoría de la
propiedad intelectual* ([TPI]{.versalita}) que explique de manera consistente todas 
sus manifestaciones --parencite(stengel2004a)--. Uno de los primeros problemas es la definición de 
«propiedad intelectual», en la cual la dicotomía idea/expresión adquiere cierta 
relevancia, ya que de esta manera se diferencia lo intangible (la idea) de lo 
tangible (la expresión). 

En este escrito se intentará demostrar cómo, a pesar de que la dicotomía puede
tratarse de manera platónica (específicamente desde la tesis de un mundo de las ideas
independiente al mundo sensible), el concepto de [PI]{.versalita} obedece a una interpretación de la
realidad más apegada al pensamiento moderno, lo cual puede ayudar a relacionar 
el incremento de la importancia económica de la [PI]{.versalita} con la gestación 
de un quehacer cultural cada vez más individualizado.

## La dicotomía idea/expresión

\noindent La definición más habitual de la [PI]{.versalita} es «la expresión concreta de
pensamientos e ideas» --parencite(ompi2016a)--. En México, podemos encontrar un ejemplo de esta definición
en la Ley Federal del Derecho de Autor, en la cual se señala que la protección comienza
cuando la obra ha sido «fijada» en un soporte material --parencite(indautor1996a)--, dando a entender
que existen al menos dos momentos en la creación literaria: la idea de una obra 
y su concreción mediante la escritura.

Esta dicotomía también ha facilitado la delimitación de la [PI]{.versalita} para
evadir la crítica de que los derechos de [PI]{.versalita} buscan la apropiación
de ideas. En este sentido, se dice que *los derechos de [PI]{.versalita} protegen
expresiones concretas, no ideas* --parencite(moore2014a)--.

En la exploración bibliográfica que se ha llevado a cabo, por la cual se han revisado
diversos clásicos dentro del debate sobre la justificación de la [PI]{.versalita},
no se ha encontrado ninguna argumentación sostenida que defina lo que se quiere dar
a entender por «idea» o por «expresión». En su lugar, los teóricos de la [PI]{.versalita}
han enfocado sus esfuerzos en sostener, con diversos matices, la definición habitual
a partir de autores como Locke --parencite(moore2012a)--, Kant --parencite(stengel2004a)--, 
Hegel --parencite(schroeder2004a)-- o autores utilitaristas --parencite(hughes1988a)--.

Bajo esta situación, al menos como un ejercicio, parece válido vincular concepciones
platónicas a la definición de la [PI]{.versalita} que se basa en la dicotomía idea/expresión.
Si se parte del supuesto de la existencia de esta dicotomía sin la mención explícita
de su origen, es posible argumentar que la idea puede interpretarse como preexistente 
y no solo distinta a su expresión; *en este sentido, el mundo de las ideas puede ser el punto
de partida para definir la [PI]{.versalita}*.

## La idea preexiste a la expresión

\noindent En el *Fedón*, Platón a través de Sócrates expone su teoría de las ideas (95a-107b). 
Cabe recordar que esta teoría sostiene la existencia de dos realidades ontológicas:
una simple, invisible, invariable e inmaterial y otra compuesta, visible, cambiante 
y material. Este segundo reino es el del mundo sensible: una especie 
de reflejo de aquel otro mundo igual en sí mismo que es el de las ideas, también 
llamadas «formas». Las ideas no solo forman parte de una realidad distinta a los 
objetos sensibles, sino que también son una realidad preexistente al mundo tangible; 
de no ser así, no podría explicarse cómo a partir de los objetos materiales y cambiantes 
es posible obtener un conocimiento invariable e intangible. Esta relación ontológica 
entre ambos mundos se podría comprender gracias a la dualidad alma/cuerpo que presentan
las personas, por la cual el alma, al presentar características afines a las ideas, 
preexiste al cuerpo. Dicha preexistencia no solo justifica la inmortalidad e indestructibilidad 
del alma, sino que también funda la noción de «conocer es recordar»: es gracias 
a la reminiscencia que tenemos acceso al conocimiento de las ideas. Aunque el acto 
de conocer es regresivo, llevando del mundo sensible al de las ideas, el mundo de las ideas 
antecede a los objetos sensibles y es la luz para un mundo de sombras.

Al decir que la [PI]{.versalita} es la *expresión* concreta de una *idea* es posible
asociar esta definición con la teoría platónica de las ideas. Por un lado, la *idea*
se presentaría con las características de simpleza, invisibilidad, invariabilidad 
e inmaterialidad, mientras que la *expresión* se asemejaría a esta realidad compuesta,
visible, cambiante y material. Esto arroja como resultado que las ideas no le pertenecen
a nadie, por lo que no pueden ser tratadas como una propiedad sujeta a derechos,
delimitación que, como se mencionó, es empleada para argumentar que los derechos 
de [PI]{.versalita} son relativos a la expresión de una idea y no a las ideas en
sí mismas. No obstante, esto también arroja dos cuestiones interesantes.

En primer lugar, si bien el *Fedón*
tiene la particularidad de ser un diálogo en el que Sócrates no muestra su habitual
falta de compromiso --parencite(zhu2005a)-- o establece tesis que no son compartidas por otros
diálogos, como la dualidad alma/cuerpo --parencite(platon1)-- —dificultando su relación con otras 
partes de la obra platónica—, cabe la posibilidad de ahondar en la delimitación de 
la expresión distinguiendo dos tipos de conocimiento: *doxa* y *episteme*.

En la *República*, Platón menciona dos tipos de conocimiento (Libro V):

1. uno que yace entre el conocimiento y la ignorancia, entre lo que es y no es,
   variante y característico de la creencia, que llama *doxa*, y
2. otro que no presenta esta ambivalencia y propio de un conocimiento
   racional, que cataloga como *episteme*.

\noindent Lo relevante de esta distinción es que en la búsqueda de aprehensión de las
ideas por lo general se aprehenden *copias*, imitaciones que incluso en algunos
casos suplantan a las *ideas*. En este sentido, ciertos tipos de poesía son desestimados
por Platón por su mimeticidad y su facilidad de pasar por alto las ideas --parencite(karasmanis1988a)--.
Solo a través de cierta preparación es posible acceder paulatinamente al ideal 
de aprehensión de formas mediante la *episteme*.

Si en la dicotomía idea/expresión se asocia el término «idea» con una concepción
platónica, la expresión puede entenderse como una participación
de estas no solo por lo que tienen de semejante, sino también por lo desemejante.
La expresión sería cualquier participación de la idea en el mundo sensible; es decir,
la «concretud» sería irrelevante. La idea sería parte del mundo de las formas, 
mientras que la expresión sería cualquier manifestación del mundo sensible, desde
la «fijación» de la idea en un soporte material hasta el acto de pensar esta idea.
La [PI]{.versalita} abarcaría no solo objetos, sino también pensamientos, ya que
el pensamiento es distinto e inferior a la idea. Esto daría resolución a algunas 
dificultades dentro de la [TPI]{.versalita}, como la posibilidad de que
los algoritmos estén sujetos a derechos de [PI]{.versalita} --parencite(hughes1988a)--.

Pero también se desata una segunda cuestión: no debe de existir el control de la
[PI]{.versalita}. Como se mencionó con anterioridad, es necesaria cierta preparación
para poder pasar de la *doxa* a la *episteme*, donde una perspectiva ética y pedagógica
adquiere relevancia.

También en la *República*, Platón vuelve a retomar la teoría de las ideas con la
alegoría de la caverna (Libro VII, 514a-520a). En esta es posible distinguir cuatro 
estados --parencite(karasmanis1988a)--:

1. El prisionero solo observa sombras sobre un muro.
2. El prisionero cae en la cuenta de que está viendo marionetas.
3. El prisionero se revela y se percata que son la reflexión de objetos reales, afuera de la caverna.
4. El prisionero sale de la caverna y se encuentra con las ideas.

\noindent Es por el traslado de estos estados que la alegoría de la caverna se considera
una paradoja sobre la educación --parencite(smith1999a)-- o una analogía de la condición humana
sobre la educación o su ausencia --parencite(karasmanis1988a)--. La educación se fundamenta como el arte
de la orientación (518d3-5), en lugar de un acto de introducción de conocimientos
(518b6-c2). No solo se mantiene la tesis de que conocer es recordar, sino que también
existe la responsabilidad por la cual la educación es liberación --parencite(losion1996a)--.

Para Platón, las personas aptas para la filosofía son aquellas que tienen la capacidad 
de ayudar a otros en la reorientación de su mirada, para dejar de suponer que la sombra
de la idea es el objeto real. Por amor a la sabiduría, esta facultad pedagógica
ha de llevarse a cabo sin un intercambio económico de por medio. Si Platón también
desestima la labor educativa de los sofistas es debido a que, en su opinión, ellos 
no solo carecen de verdadero conocimiento (*episteme*) sino que también buscan
un intercambio económico para facultar a otros el acceso a esta clase de 
conocimiento --parencite(losion1996a)--.

En la definición de la [PI]{.versalita} a partir de Platón, se tendría como resultado
que la segunda dimensión de la [PI]{.versalita} no sería para justificar el control 
de la creación que posibilita una economía de la [PI]{.versalita}. Los derechos de 
[PI]{.versalita} se traducirían como derechos relacionados con el libre acceso a la información, 
de manera que los creadores de [PI]{.versalita} tendrían la responsabilidad de hacer 
accesibles los frutos de su trabajo. De este modo, la tercera dimensión de la 
[PI]{.versalita} se constituiría como un sistema encargado de velar por esta 
libertad de acceso.

## La idea solo es distinta a la expresión

\noindent Como es evidente, la asimilación del concepto de «propiedad intelectual» con la
teoría de las ideas de Platón es conflictiva ya que supone una serie de supuestos
que en la actualidad son difíciles de aceptar. Estos pueden ser:

* La dualidad ontológica entre el mundo de las ideas y el mundo sensible.
* La preexistencia e independencia del mundo de las ideas.
* La persona como dualidad alma/cuerpo.
* La preexistencia e independencia del alma con relación al cuerpo.
* La posibilidad de transmigración del alma.
* Un sistema de recompensa que justifica la pertinencia de purificar el alma.

\noindent Si bien el reconocimiento de estos supuestos depende de la interpretación que se le
dé al *Fedón* o a la alegoría de la caverna, el carácter mínimo requiere
la aceptación de una dualidad ontológica, e incluso antropológica. La definición 
de la [PI]{.versalita} en clave platónica depende de la disposición 
a dar concesiones a estos supuestos.

A la par que la versión platónica de la [PI]{.versalita} palidece por la dificultad
que representa la defensa de dos mundos, se hace posible evidenciar dos aspectos. 
El primero es que, a pesar de que no existe una mención explícita sobre el origen 
de la dicotomía idea/expresión en la que descansa la definición de la [PI]{.versalita}, 
se hace patente que al menos el término «idea» es de connotación moderna. Es por ello
que varios intérpretes de Platón prefieren hablar del mundo de las
formas, en lugar de las ideas, para evitar esta confusión. Así, también
se manifiesta por qué en la [TPI]{.versalita} no existe una explicación
sostenida sobre lo que se entiende por «idea» y por «expresión»: está supuesto en 
el trato que se le da al concepto a partir de filósofos modernos como Locke, Kant,
Hegel o los utilitaristas. Esto permite identificar la génesis del concepto de 
«propiedad intelectual» en la tradición moderna del pensamiento occidental.

El segundo aspecto que puede desprenderse es que este concepto tiene una intención 
bien definida: la justificación «filosófica» de la [PI]{.versalita}, como es sostenida 
por varios de sus teóricos --parencite(hettinger1989a,hughes1988a,shiffrin2007a,stengel2004a,drahos1996a)--, 
carece de sentido si no ayuda a respaldar el 
control de la creación desde un plano jurídico, lo cual es la doctrina del derecho 
de la [PI]{.versalita}, como las patentes o los derechos de autor.

El trato platónico de la [PI]{.versalita} daría como resultado la inutilidad de 
los derechos de la [PI]{.versalita} desde el plano jurídico. Al ser los derechos
de [PI]{.versalita} asimilados al derecho de acceso a la información, los derechos
económicos serían irrelevantes e incluso conflictivos, así como la apropiación indebida 
o malversación de la [PI]{.versalita} podría tratarse desde otras doctrinas jurídicas 
como son las concernientes al plagio, a la suplantación de identidad o a la difamación. Esto
solucionaría un problema presente en los derechos de [PI]{.versalita}: en varios
casos existe la dificultad de que pretende legislar aspectos que ya forman parte 
de otras doctrinas jurídicas.

Al ser innecesaria la segunda dimensión de la [PI]{.versalita}, la tercera
dimensión también se mermaría, al no haber un conjunto de derechos por 
sistematizar. La [PI]{.versalita} se reduciría a su primera dimensión: la expresión
concreta de una idea. Esto permitiría cierta flexibilidad en el uso del término. La 
creación se constituiría como una «propiedad» de la cual el creador no podría tener 
control económico sino el deber de cederlo a los demás, haciendo posible
prescindir del término «propiedad intelectual» y en su lugar emplear una expresión
de mayor amplitud, como lo es «creación intelectual».

Sin embargo, en la [TPI]{.versalita} la sinonimia entre «creación» y 
«propiedad» es conflictiva. La constitución de la propiedad es el vínculo que permite 
hablar de derechos económicos mediante el control de uso de la creación. La 
definición de la [PI]{.versalita} precisa de la distinción entre «creación» y 
«propiedad», para así sostener el resto de las dimensiones.

La versión platónica de la [PI]{.versalita} no solo dificulta este nexo, sino que
suprime las otras dimensiones. La falta de aceptación de esta definición
de la [PI]{.versalita} no es solo por los supuestos que implica la teoría platónica
de las ideas, sino también porque es contraproducente a la intención que se busca 
en su justificación; a saber, fundar el control al menos económico de la creación.

Una de las justificaciones de la [PI]{.versalita} que permite la fundación del
control económico es la concerniente a la aplicación del término «propiedad» para
las creaciones intelectuales. Para este caso diversos teóricos de la [PI]{.versalita}
se valen de Locke --parencite(moore2012a,hughes1988a,stengel2004a)--, específicamente del 
capítulo «De la propiedad» del *Segundo Tratado sobre el Gobierno Civil*.

De manera general, en este capítulo Locke justifica cómo es posible hablar de 
propiedad privada a pesar de que Dios le otorgó la tierra a todos los hombres. El 
traslado de un bien común a la propiedad es mediante el esfuerzo. El hombre 
que mediante el esfuerzo de su labor cultiva la tierra tiene el derecho de obtener un 
beneficio económico por los frutos de su trabajo, siempre y cuando deje suficiente
para los demás (25-51).

En el plano de la propiedad física, las ideas lockeanas son difíciles de asimilar
debido a la ambigüedad presente en la expresión «suficiente para los demás» en un contexto
finito, como es la repartición de tierras --parencite(locke2006a)--. Sin embargo, en la búsqueda de
una justificación de la [PI]{.versalita} esta indicación de Locke se considera 
más afín, debido a que las ideas son infinitas, por lo que siempre hay «suficiente
para los demás». De esta manera se posibilita que a partir de Locke se hable de
[PI]{.versalita}, ya que la apropiación no entra en conflicto con la oportunidad
que tienen otros de apropiarse de otras ideas, debido a su inagotable particularidad.

Aquí es posible hacer una observación. Como varios de los teóricos de la [PI]{.versalita}
han apuntado, la explicación de Locke se centra en una relación entre la tierra 
y el cultivo, por lo que una asimilación con la idea y la expresión dentro de la
[PI]{.versalita} puede no ser adecuada --parencite(stengel2004a)--. A pesar de esto, se afirma
que la asimilación es posible. En lugar de «tierra» y «cultivo» se puede
hablar de «ideas» y «expresiones» respectivamente. Con esto, así como la tierra
(léase «ideas») está a disposición de todos los hombres, es el esfuerzo involucrado
para producir un cultivo (es decir, «expresiones concretas») lo que permite al
agricultor (aquí «creador») la apropiación y el control económico de sus frutos 
(«creación»).

La analogía es interesante; más si se toma en cuenta que alimenta el constante
imaginario donde el creador tiene un vínculo estrecho con su creación, así como
el agricultor premoderno sostiene un nexo cercano con las plantas de su cultivo.
Pero presenta una dificultad. En Locke, la «tierra» y el «cultivo» no solo son elementos
distintos, sino que también existe una relación ontológica de dependencia donde la
«tierra» preexiste, como una creación de Dios otorgada a los hombres, al «cultivo», 
ese fruto directo por el esfuerzo del hombre.

Incluso esta relación de dependencia se sostiene en ausencia del supuesto de que
la tierra es una creación de alguna entidad divina, porque no puede haber cultivo 
si previamente no se cuenta con la tierra, como es perceptible en las frecuentes
disputas entre campesinos o ejidos. Para la analogía realizada por los teóricos de 
la [PI]{.versalita}, se indica entonces que, de ser posible, implica que la idea 
no solo es distinta a la expresión, sino también preexistente: no hay expresión
sin ideas previas por apropiar.

Aquí se presenta una disyuntiva. Si se desestima la versión platónica de la [PI]{.versalita}
debido a que precisa de la tesis donde la idea preexiste a la expresión —y ya no solo
porque sus consecuencias son contraproducentes para la intención original de la [TPI]{.versalita}—, 
entonces también se ha de rechazar la teoría de la propiedad 
de Locke para la justificación de la [PI]{.versalita}, ya que del mismo modo implica 
el supuesto de que la idea preexiste a la expresión. O bien, si se aceptan los razonamientos
lockeanos para la definición de la [PI]{.versalita}, del mismo modo es teóricamente
sostenible una justificación platónica de la [PI]{.versalita}, sin que la tesis
de la preexistencia de la idea se presente como un elemento débil para este argumento.

Dentro de la [TPI]{.versalita} los argumentos lockeanos son importantes
para defender los derechos económicos de la [PI]{.versalita}. Kant y Hegel son 
referidos para otro aspecto de la teoría, concerniente al supuesto vínculo inalienable
entre el creador y su creación, lo que en el ámbito jurídico se conoce como derechos
morales --parencite(moore2014a)--. Por otro lado, las justificaciones utilitaristas son empleadas
para poder pasar de la esfera subjetiva al ámbito social, señalando que el control 
del creador sobre los derechos económicos de su creación se traduce en alguna clase 
de beneficio social --parencite(hughes1988a)--, como queda patente en la relación entre el crecimiento 
económico y la [PI]{.versalita} que sostiene la [OMPI]{.versalita}.

Si la alusión a Locke implica una analogía entre la tierra/cultivo y la idea/expresión,
la preexistencia de la idea es un supuesto necesario. Tal suposición abre la puerta
a otras justificaciones filosóficas de la [PI]{.versalita}, como la platónica, que
merman la intención original de control al poder desembocar los derechos de [PI]{.versalita}
en derechos de acceso a la información.

## La irrelevancia de la dicotomía

\noindent La dicotomía idea/expresión acarrea dificultades a la [TPI]{.versalita},
por lo cual se sostiene que quizá esta distinción es innecesaria --parencite(stengel2004a)--.
Con base en lo dicho aquí es posible indicar que la justificación de la [PI]{.versalita} 
hace referencia a los frutos del acto creativo, principalmente en lo concerniente 
a su control. Mientras tanto, la dicotomía idea/expresión parece hacer referencia 
a un fenómeno dentro del acto creativo, por lo cual es previo a la [PI]{.versalita}, 
quedando afuera de la esfera de interés para la [TPI]{.versalita}.

Suponiendo esto, el acto creativo se entendería como el traslado de la idea a la
expresión, dinámica que no siempre es satisfactoria, lo que permite delimitar la 
[PI]{.versalita} solo a aquellos casos en los que la expresión es alcanzada de manera 
plena. De esta manera podría explicarse por qué las patentes solo se otorgan si 
se trata de invenciones realizables, comprendiendo que su implementación 
debe de satisfacer la idea que busca realizar. Esto también justifica cómo las ideas 
de alguna novela o de un logotipo no pueden ser sujetas a [PI]{.versalita} a menos de que la 
obra se redacte o el logo se diseñe, ya que el acto creativo no se considera concluido 
hasta que de la idea se llega a la expresión.

Si bien esta visión del acto creativo va muy acorde al imaginario donde el creador
de alguna manera tiene la facultad, habilidad y claridad para expresar concretamente
sus ideas, capacidad que se considera escasa entre la mayoría de las personas, también
revela que la dicotomía es importante para la [PI]{.versalita}.

Cabe la posibilidad de sostener que la [PI]{.versalita} no se enfoca al acto creativo,
sino a los frutos de este. No obstante, esto no afecta la tesis de que la dicotomía
idea/expresión es relevante para la justificación de [PI]{.versalita}. Como se ha
buscado explicar en este escrito, la dicotomía es elemental para poder delimitar
los alcances y las pretensiones en la definición de la [PI]{.versalita}. El acto creativo
como principio distinto a la [PI]{.versalita} y entendido como el paso de la idea a la expresión
delimita incluso más lo que es la [PI]{.versalita}, haciendo que la dicotomía sea relevante 
al indicar que, dentro de los actos creativos, solo los que cumplen este traslado 
están sujetos a derechos de [PI]{.versalita}. La dicotomía no solo supone lo que es 
el acto creativo, sino también lo que es la [PI]{.versalita}.

## La relevancia de la dicotomía

\noindent La dicotomía no puede ignorarse en la [TPI]{.versalita}. De hacerlo, 
los argumentos lockeanos carecerían de fuerza, ya que su principal fundamento es 
una analogía que implica una relación entre la idea y la expresión. Sin Locke, la 
[TPI]{.versalita} no puede sostener los derechos económicos de la [PI]{.versalita} 
como se dan en la actualidad, los cuales a su vez justifican su pertinencia social desde 
la vertiente utilitarista. En la ambigüedad y la asimilación de la [PI]{.versalita}
como propiedad privada se posibilita la transferencia de la propiedad más allá del
creador «original».

La [TPI]{.versalita} únicamente podría sustentar la obtención de beneficios 
económicos directos para el creador mediante Kant o Hegel. Estos consideran que 
la obtención de una remuneración es importante para la creación al permitir el
desarrollo de la personalidad --parencite(hughes1988a)--. Es decir, toda economía de [PI]{.versalita} 
que se transfiere a entidades estatales o privadas estaría injustificada, ya que 
las instituciones no son personas. Por otro lado, tampoco sería posible la prolongación
de los derechos de [PI]{.versalita} más allá de la muerte del creador, debido a 
que ya no existiría personalidad que desarrollar.

Para evadir esta complicación, es posible argumentar que la preexistencia de la idea
en la analogía realizada a partir de los razonamientos de Locke tiene una característica
ontológica distinta a la de Platón. Con el ateniense la «preexistencia» se entiende
como la existencia de dos mundos, mientras que con el inglés la «preexistencia»
no implicaría la existencia de más de un mundo, ni la «preexistencia» en la nada,
por el carácter paradójico que esto implica.

*Si la idea no preexiste en un mundo distinto, ni en la nada; si la expresión se
refiere a objetos «concretos» cuyos usos se pueden controlar; entonces, la única 
preexistencia posible de la idea es en el sujeto*.

La importancia del sujeto para la filosofía moderna es ya sabida. Lo que
se desea resaltar en este caso es que solo mediante la apelación del sujeto es 
posible habilitar el argumento de Locke para la definición de la [PI]{.versalita},
al mismo tiempo que impide abrir la puerta a otra clase de justificaciones,
como la platónica. Con esto se evidencia que la [PI]{.versalita} obedece a una 
interpretación muy concreta de la realidad en la que el acto creativo —al igual que el quehacer
cultural en general— es principalmente una relación entre un sujeto creador y
el objeto creado.

No es la intención de este trabajo ahondar en la relación sujeto/objeto, sino
la de exponer la dependencia de la [PI]{.versalita} a una perspectiva individual
de la cultura, que al mismo tiempo permite explicar los motivos por los que la 
[PI]{.versalita} no es relevante en los siguientes contextos:

* En la Antigüedad, porque el acto creativo y sus frutos no contemplan una concepción
  que involucra la noción de «sujeto».
* En la escolástica, debido a que la fuente de las ciencias y las artes no tienen
  su origen en un sujeto, sino en Dios.
* En la cultura popular, ya que el acto creativo no se centra en un sujeto, sino
  en la herencia y en la modificación colectiva de las creaciones intelectuales.
* En algunas dinámicas culturales posibilitadas por la era digital, porque se 
  facilita la producción colectiva o la «disolución» del sujeto a través de actos 
  creativos como el *remix*, el *collage* o el pastiche.

\noindent De ser cierto que la [PI]{.versalita} es esencialmente una relación sujeto/objeto,
es posible comenzar a definir pautas para entender la «visión del mundo» que supone tal
concepto y que permitirá profundizar en esta cuestión, que se ha catalogado como 
la «justificación filosófica de la [PI]{.versalita}».

---

\begin{flushleft}
Autor: Ramiro Santa Ana Anguiano. \\ Editora: Mariel Quirino Andrade. \\ Redactado: marzo del 2017. \\ Última modificación: noviembre del 2017. \\ Escrito bajo \href{https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre}{Licencia Editorial Abierta y Libre}. \\ Contenido disponible en \href{http://xxx.cliteratu.re/}{xxx.cliteratu.re}.
\end{flushleft}
