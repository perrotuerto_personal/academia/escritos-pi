# ¿Propiedad intelectual antropológica? 

\vskip 3em

\noindent En las últimas décadas la economía basada en la propiedad
intelectual ([PI]{.versalita}) ha tenido un crecimiento sin precedentes.
Los derechos de autor, las patentes, los secretos comerciales
o las denominaciones de origen llevan la batuta. ¿Quién
podría haber imaginado que el _top_ de las empresas a
nivel mundial no fundarían su riqueza mediante la manipulación de
materias primas? ¿Cuántos hubieran pensado que las empresas con
mayor margen de ganancias serían las que _solo_ proveen
servicios? Compañías de desarrollo tecnológico cuya moneda de
cambio son los bits y la información: la dichosa economía de
servicios.

La intangibilidad de lo que ofrecen ha puesto en el centro
del debate jurídico temas relacionados a la privacidad y
la [PI]{.versalita}. Sin mención expresa, la encrucijada de la
[PI]{.versalita} se enfoca en el autor/inventor como productor.
Mediante procesos creativos o técnicos _una persona produce_ las
cosas que dan forma a nuestro mundo. Con la legislación se pretende
proteger esta actividad tan íntima, tan erótica, tan intrínseca
entre _el_ autor y _su_ obra; _el_ inventor y _su_ invento:
_el_ productor y _su_ producción.

Y aunque existen elementos para sospechar que la [PI]{.versalita}
no es un asunto privado, sino de índole institucional, este nexo
íntimo pervive sin perder su vitalidad. A través de compañías,
gobiernos o universidades la [PI]{.versalita} se engrosa, se alimenta
y se reproduce a sí misma. Por medio de cada uno de nosotros la
[PI]{.versalita} encuentra los fundamentos para su reproducción
al asentir en un tipo de relación extraña y hasta narcisista entre
el productor y su producción.

La «aura» descrita por Walter Benjamin --parencite(benjamin2011a)--
podría ser una aproximación. El «aura» para Benjamin crea ese halo
sobre la obra de arte que evita reducirla en un objeto de
uso cotidiano o revolucionario. Algo extraño y sublime rodea a
ese tipo de obra que, siendo heterodoxos con este concepto, también
es perceptible en otras producciones, como la literaria, la filosófica,
la científica o del hombre.

Los modos de producción han construido el mundo no solo a partir
de la elaboración de cosas, sino también de personas. Desde
hace tiempo se ha constatado que la subjetividad, sagrada para
el pensamiento moderno, también se fabrica. No hay nada de
«natural» ni de espontáneo, sino que surge a partir de una
serie de procesos estructurales que moldean al individuo.
Sin embargo, ¿qué más importa? El «aura» del producto antropológico
se sigue sosteniendo.

Bajo el reinado del aura, el ser humano tiene una naturaleza o
condición que lo hace ser él mismo. Un remanente que lo liga a una
esencia, a una cultura o a una historia, por más débil que sea.
Un residuo que constantemente entorpece a la maquinaria que
anhela la fabricación antropológica masiva.

La concepción de una «impresión de sujetos en una prensa»
--parencite(sloterdijk2012a)-- no solo causa desconcierto, genera
desasosiego porque despoja al hombre de lo que en su propia cultura
ha defendido como lo más importante: la producción. En un primer término,
al ser la materia prima, el producto final y el productor, se difumina
el límite que hemos erigido entre la producción y el productor. Este,
sin mención expresa, se había distinguido por ser irreducible a la
producción. A lo sumo podía percibirse como una _extensión_ de
su yo.  Por otro lado, la producción muta supuestos en hechos
al constituirse como _objeto_ externo o diferenciable de la
biología de la especie humana.

La cultura ante ojos modernos y bajo el reinado del aura es un
quehacer que produce objetos, sean sublimes u ordinarios.
El hombre _produce_ no solo lo que lo rodea, sino también su
futuro y su pasado. El ser humano fabrica de manera incesante
su porvenir y su pasado.

Pero hace tiempo la categoría de «producción» se ha amplificado.
Ya no solo se elabora todo aquello que rodea, se le incrusta
o marca el tiempo del hombre, este también es un producto.
Ya no es esencia ni condición, es técnica, es maquinaria.

Desde una perspectiva teórica esta amplificación hace preocuparnos
por distinguir y matizar los tipos de sujetos producidos, las
maquinarias que los elaboraron y los supuestos por los cuales los
sujetos fueron fabricados de una manera y no de otra. En el
control de los medios de producción que el análisis teórico
consigue, la curiosidad intelectual muda en experimentación.
Del fundamento del mundo como construcción social o cultural
el intelectual pasa a proponer nuevos o súper modelos de hombres.
En el discurso de la construcción del sujeto se abre paso a las
propuestas de generación de otros modelos de subjetividades.
En la labor empírica y la curiosidad se generan individuos que
incluso se fundan a través de autoidentificaciones que hacen
desechable los aparatos sociales y culturales que los producen:
de la _producción_ a la _creación_ espontánea, fluida y plástica
del individuo _per se_.

Pero ¿qué se obtiene con esto? ¿No puede implicar una diversidad
ficticia; sujetos _modernity reloaded_, subjetividades a modo de
distintas marcas y sabores pero pertenecientes a los pocos
conglomerados industriales cuyos productos nos ofrece Soriana?

No es conservadurismo que teme producir algo nuevo. Es la
pregunta si es cierto que lo fundamental de nuestra cultura ha
sido la producción de cosas o de hombres. Es una impugnación de
cómo es posible producir sin transformar, «incluso cuando los
materiales con que se abastece parecen ser de naturaleza
revolucionaria» --parencite(benjamin2004a)--.

La pérdida del aura y su carácter sublime de la producción
también pone en entredicho la relevancia de la producción misma.
¿Qué amplifica el mundo sin transformarlo? ¿Qué permite una
acumulación incesante de capital sin necesidad expresa de un
crecimiento exponencial de cosas y hombres _distintos_? ¿Qué hace
necesario una uniformidad en la producción, incluyendo al producto
antropológico, para la estabilidad política y económica?

La reproducción siempre se ha considerado como un aspecto secundario
en nuestra cultura. La reproducción hasta ha sido tildada de
negatividad. Sin embargo, la necesidad cultural de materias primas
uniformes, de estándares, y de medias, medianas y modas hacen patente
que la característica del quehacer cultural es la reproducción.
Si a esto se suma que los modos de reproducción hacen posible la
fabricación masiva, las velocidades de producción quedan a un lado
por la versatilidad, eficacia y eficiencia de la reproducción. Se
crean superhéroes, pero es en su reproducción en forma de cómics,
películas, figuras de acción, _souvenirs_, _fanart_, _cosplay_ y la
generación de subjetividades en como una idea tan bruta se pule y
robustece.

La modelación del mundo en una lucha maniquea en donde cada individuo
se percibe como la clave para el fin del conflicto es un producto
de la reproducción cultural que va de la elaboración de objetos
a la fabricación de sujetos. No es extraño ni despreciable,
la reproducción moderna de nuestra cultura busca sacar al mercado
una serie de subjetividades, diversas, sin duda, pero con poca
capacidad de transformación. El sujeto revolucionario, como la
literatura revolucionaria, convierte la lucha política en objeto
de satisfacción, en un artículo de consumo, en un espectáculo
de variedades --parencite(benjamin2004a)--.

En una economía de servicios se indica que al consumidor se le
ofrecen gratuitamente los objetos, ya que la acumulación de capital
reside en las actividades que un tercero ofrece para aumentar el
goce de las cosas. En la crítica a la «gratuidad» de los productos los
críticos en pos de la privacidad argumentan que el objeto de
consumo es el propio consumidor y la información que genera pero
que no le pertenece. El consumidor paga su deuda al ceder y
constituirse como su objeto de goce.

Desde la perspectiva de la regulación de la [PI]{.versalita} se habla
de una flexibilización. La apertura empieza con el ofrecimiento
gratuito del producto. Sin embargo, la flexibilidad _cool_ es
aquella que no solo da acceso sin restricciones al producto final,
sino que otorga las configuraciones y los elementos necesarios para su
reproducción y modificación. La [PI]{.versalita} no se destruye solo
se transforma. Sus impedimentos ya no son jurídicos o políticos,
sino meramente técnicos. ¿Quién puede constituirse como un «mejor»
sujeto? Aquel con mayor capacidad técnica. La documentación está ahí,
basta con tener los conocimientos y el control de los medios de
reproducción para poner a trabajar a la maquinaria antropológica.

La «maquinaria antropológica» y el «producto antropológico», así como
la [PI]{.versalita} como medio de regulación de los aparatos
reproductivos de nuestra cultura y de las posibilidades de producción
de «nuevo» objetos, hacen posible ampliar los límites de la reflexión
entorno a la [PI]{.versalita} y nuestra cultura. Si la antropotécnica
pretende las «reproducciones perfectas de hombres» --parencite(sloterdijk2012a)--
cuya base es el «antagonismo simbiótico entre el acreedor y el
deudor» --parencite(sloterdijk2012a)--. Si el «aura» dota de un halo que
incapacita la percepción maquinal de la obra producida --parencite(benjamin2011a)--.
Si en la imposibilidad de desarticular la producción se pierde de vista
los intereses e intenciones del dueño de la maquinaria, así como la
posibilidad revolucionaria --parencite(benjamin2004a)--. Entonces cabe
la pena preguntarse la pertinencia de la hipótesis donde la [PI]{.versalita}
en mayor disputa no son ya los derechos de autor, las patentes, los
secretos comerciales o las denominaciones de origen, sino la vida misma,
comenzando por la vida humana.

La [PI]{.versalita} por excelencia sería la _propiedad intelectual
antropológica_. No caduca de manera artificial, su valía tiene como límite
el accidente o la muerte. No requiere elementos para producirla o sostenerla,
por sí sola encuentra los medios para sobrevivir. No solo eso, esta misma
busca el modo de engrosarse, de hacerse más compleja y, por ende, más
valiosa. Lo que es aún mejor, como imán esta [PI]{.versalita} se aproxima
a los centros de acumulación de capital. La industria y la cultura no tiene
que buscarla ni generarla, solo poseerla mediante la deuda. En su
reproducción y ampliación la [PI]{.versalita} antropológica se vale del
crédito. En la cesión de derechos de los productos de su trabajo esta paga
su deuda. En su afán de no ver más allá del halo creado por su aura, este
tipo de propiedad ignora a cuáles intereses obedece y su ubicación en la
maquinaria de producción masiva y uniforme de hombres. Pero, principalmente,
olvida, minimiza o ridiculiza su potencial revolucionario en pos de la
industria o la cultura a las que se ve identificado y sujeto.

Hasta que la [PI]{.versalita} antropológica no pueda ser sustituida
por la inteligencia artificial, esta es el androide que reproduce
la economía y la política en voga a escala planetaria. La economía de
servicios se trataría como una economía basada en el sudor ocultado de la
[PI]{.versalita} antropológica. Se trata de una apuesta de análisis
arriesgado, como quedó demostrada en esta «caricatura», pero una
_quizá_ vale la pena hacerla.

---

\begin{flushleft} Autor: Ramiro Santa Ana Anguiano. \\ Redactado:
diciembre del 2018. \\ Última modificación: diciembre del 2018.
\\ Escrito bajo \href{https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre}{Licencia
Editorial Abierta y Libre}. \\ Contenido disponible en \href{http://xxx.cliteratu.re/}{xxx.cliteratu.re}.
\end{flushleft}
