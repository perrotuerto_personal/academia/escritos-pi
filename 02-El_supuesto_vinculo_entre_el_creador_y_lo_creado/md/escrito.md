# El (supuesto) vínculo entre el creador y lo creado

\noindent La doctrina de la propiedad intelectual ([PI]{.versalita}), al definir su objeto, 
indica que la [PI]{.versalita} es «expresión concreta de una idea» --parencite(ompi2016a)--. En
la defensa de dicha definición por lo general se recurre a tres vertientes:

1. Algunos escritos de Locke para justificar los derechos económicos de la 
   [PI]{.versalita} --parencite(moore2012a)--.
2. Los argumentos de ciertos autores utilitaristas para justificar la importancia de los tratados 
   o acuerdos internacionales sobre la [PI]{.versalita} --parencite(hughes1988a)--.
3. Escritos de autores como Kant o Hegel para justificar los derechos morales de la [PI]{.versalita} --parencite(stengel2004a)--.

\noindent A partir de la obra de Locke se argumenta que, debido al esfuerzo empleado para crear un objeto,
el sujeto creador tiene el derecho de ser recompensado mediante un intercambio
económico con quienes desean los frutos de su trabajo --parencite(locke2006a)--. El discurso utilitarista
sirve como base para argumentar que este intercambio económico se traduce en un
mejor bienestar social. En este sentido, mediante la creación de mecanismos de 
control nacionales e internacionales se busca fomentar la economía de la [PI]{.versalita}
al ofrecerle al sujeto creador varios medios de protección y de compensación --parencite(ompi2015a)--.

Sin embargo, en el ámbito legislativo esto no garantiza que el sujeto creador
siempre conserve la creación como una propiedad. Incluso, por el mismo modelo
económico con el que se rige la [PI]{.versalita}, sería necesario que el sujeto
enajene su creación en pos de un mayor bienestar social. Esto generaría situaciones
que den pie a la suplantación u ocultación de la identidad del creador original,
yendo en contra del sistema de incentivos que se pretende con la creación de
mecanismos de control.

Para evitar este problema, la [PI]{.versalita} no solo precisa de una doctrina de
derechos económicos, fácilmente transferibles de una persona a otra, sino también
de una doctrina de los derechos morales. En general, este otro conjunto de derechos
tiene la intención de proteger al sujeto creador al justificar un vínculo
inalienable entre este y su objeto creado --parencite(indautor1996a)--. Por este motivo, sin importar 
que el sujeto creador no posea los derechos de explotación de su creación, según
la legislación correspondiente existe al menos un derecho que persiste como
vínculo entre el creador y su creación; a saber, el derecho a la paternidad, también
conocido como atribución o autoría.

Los escritos de Kant y Hegel fueron la base principal al momento de fundar los derechos morales,
ya que en sus trabajos existe una defensa explícita de la creación como fundamento
de la personalidad del sujeto creador --parencite(hughes1988a)--. Bajo esta perspectiva, el vínculo
entre la obra y el autor —ambos se enfocan en la creación literaria y filosófica
exclusivamente— no es únicamente genético: también es ontológico. El autor no 
solo es necesario para que la obra «nazca», sino que esta última es una expresión 
de su personalidad, sea como voluntad (Hegel) --parencite(schroeder2004a)-- o como discurso 
(Kant) --parencite(stengel2004a)--.

En el presente texto se buscará demostrar a través de un «ejercicio mental» cómo este 
supuesto vínculo entre el sujeto creador y el objeto tiene una base cultural muy 
amplia. En un primer momento se formalizará el argumento para evidenciar que, sea 
por un criterio de verificabilidad o de falsabilidad, esta tesis no se sostiene. 
Sin embargo, al final se retoma el mismo argumento para observar cómo en su debilidad 
se ofrece la apertura a ciertas pautas que pueden a ayudar a entender 
por qué este vínculo entre el creador y su creación forma parte de otras disciplinas 
filosóficas más cercanas a la reflexión sobre nuestra cultura e incluso a la hermenéutica.

## Formalización

\noindent Aunque Kant y Hegel son los principales autores por los que se funda el vínculo
inalienable entre el sujeto creador y el objeto creado, en muchas ocasiones pasan
desapercibidos. Cuando desde la creación colectiva empieza a cuestionarse
este supuesto nexo, existen oposiciones que se manifiestan con argumentos como
«sin Einstein la teoría de la relatividad no hubiera existido», «nadie podría haber
escrito la *Ilíada* más que Homero» o «la genialidad de Steve Jobs sentó las bases
de la industria de los dispositivos móviles táctiles».

En esta variedad de argumentos no solo existe como denominador común la
referencia a creaciones que han tenido impacto en el quehacer cultural. También se pretende
validar que el sujeto creador es una condición necesaria y suficiente para el
objeto creado y viceversa. ¿Quién conocería a Einstein, a Homero o a Jobs sin
la teoría de la relatividad, la *Ilíada* (y *La Odisea*) o la creación de una
nueva industria tecnológica? Además, ¿qué hubiera sido de la teoría de la
relatividad, la *Ilíada* o la industria móvil sin Einstein, Homero o Jobs?

El primer caso implica que cada sujeto creador depende de su objeto creado
para tener un lugar en la historia. El segundo, que hay cierta incertidumbre 
sobre cuál sería el estatus ontológico de estas creaciones si no hubieran existido sus creadores. 
Así es como se se habla de una condición necesaria y suficiente; es decir, que 
el **s**ujeto creador es tal *por y solo por* el **o**bjeto creado y viceversa.
No hay uno sin otro, lo que en lógica formal se expresa como un bicondicional --parencite(trelles2002a)--:

\vskip -0.5em
$$ s \,\Leftrightarrow\, o $$

\noindent O bien, cabe argumentar que no se trata de un bicondicional, sino un condicional
donde el **o**bjeto creado es condición necesaria y el **s**ujeto creador es condición
suficiente. Es decir, que el valor histórico de Einstein, Homero o Jobs precisa
de sus respectivas invenciones, mientras que estas solo requirieron de ellos para
su advenimiento, aunque bien otros sujetos hubieran podido crear algo con el mismo 
valor cultural a la teoría de la relatividad, la *Ilíada* o la industria móvil. 
De manera formal se expresaría como:

\vskip -0.5em
$$ s \,\Rightarrow\, o $$

\noindent Para que el bicondicional *s* $\,\Leftrightarrow\,$ *o* sea verdadero, ambos términos necesitan tener 
el mismo valor: o ambos son verdaderos o ambos son falsos. Si uno presenta un valor distinto,
quiere decir que no existe semejante tipo de relación. Por ejemplo, la relación
entre Einstein y la teoría de la relatividad sería falsa como bicondicional si
Einstein fuese históricamente relevante no por su actividad teórica, sino por
haber sido un gran jugador de futbol. O bien, este bicondicional también sería falso 
si la teoría de la relatividad hubiera permanecido intacta independientemente de que
Einstein hubiera trabajado en ella.

Mientras tanto, el condicional *s* $\,\Rightarrow\,$ *o* es falso si *s* es verdadero y *o* es falso.
La relación entre Homero y la *Ilíada* sería falsa si la relevancia histórica de
Homero se mantuviera aunque no hubiera sido poeta. Sin embargo, la relación aún
sería verdadera si algo similar a la *Ilíada* fuera culturalmente relevante, 
sin importar que Homero no lo hubiera escrito.

Ante esta situación se han de buscar condiciones de verdad que constaten
la validez lógica de *s* $\,\Leftrightarrow\,$ *o* o, si se es más flexible, de *s* $\,\Rightarrow\,$ *o*. 
Históricamente podemos considerar que al menos Einstein es relevante por la teoría de la relatividad, 
Homero es importante por la *Ilíada* y Jobs por haber tenido la visión para generar
una nueva industria. En todos los casos tenemos que *s* y *o* son 
verdaderos, por lo que lógicamente —se trate de un bicondicional o condicional— la 
relación entre *s* y *o* es verdadera.

## Verificabilidad y falsabilidad

\noindent Esta validez lógica no demuestra que tales relaciones sean objeto
para una consideración científica. Aquí viene a tono uno de los problemas dentro 
de la filosofía de la ciencia que aborda su demarcación: ¿qué puede considerarse 
ciencia y qué no?

Uno de los primeros criterios, defendido en un principio por varios 
positivistas --parencite(moulines2015a)--,
fue que la verificabilidad es la base de toda ciencia. Sin verificación no es posible 
que un enunciado forme parte del *corpus* científico --parencite(popper1934a)--. Este criterio implica 
la posibilidad de reproducir el «ejercicio» una y otra vez para corroborar que 
se obtiene el mismo resultado o para desechar la hipótesis si sucede lo contrario.

En nuestro caso, ninguna de las tres relaciones que se han usado a modo de ejemplo
es verificable. Se trata de hechos históricos que no pueden ser «reproducidos».
No es posible replicar un mundo semejante al nuestro para poder verificar si en 
todos los casos obtenemos las mismas condiciones de validez lógica que las 
obtenidas en *este* mundo.

Incluso de ser posible, podría argumentarse que semejantes ejercicios
son insuficientes, debido al problema de la inducción --parencite(popper1934a)--. El criterio de 
verificabilidad implica una cantidad inagotable de experimentos para poder dar 
validez científica a un enunciado, pero no evita que un 
solo resultado contrario derrumbe años de experimentación y de supuestos que
permiten la edificación de la ciencia.

En el caso de este ejercicio, implica que no importa cuántos mundos sean
replicables para comprobar la validez de las tres relaciones. Si tan solo uno en 
un millón da un resultado contrario, no existe certeza para considerar que estas 
relaciones son un bicondicional o un condicional válido.

Ante este problema, Popper propuso otro criterio que en español se conoce como
«falsabilidad» --parencite(popper1934a)--. En lugar de tratar de verificar una hipótesis, hay que buscar
situaciones donde pueda ser falsa, lo cual puede ahorrar años de experimentación
y permite indicar qué tan sólida es una hipótesis --parencite(cardoso2009a)--. En este contexto
un enunciado es científicamente más válido si se demuestra que ha resistido esta
clase de pruebas.

Sin embargo, estas tres relaciones tampoco son falsables por su carácter histórico.
No es posible replicar un mundo semejante al nuestro pero sin Einstein, Homero
o Jobs, o sin la teoría de la relatividad, la *Ilíada* o la industria móvil, para
así observar si la hipótesis de un vínculo intrínseco entre el sujeto creador y
el objeto creado subsiste bajo esas condiciones.

*Si la ciencia se caracteriza por la verificabilidad o la falsabilidad; si en
ninguno de los casos este ejercicio mental es corroborable o refutable, entonces
la tesis de un vínculo más allá del genético entre el creador y su creación es
insostenible como objeto de conocimiento*.

## ¿Ciencia empírica o hermenéutica?

\noindent Desde sus inicios el criterio de falsabilidad fue criticado por su alto compromiso
empirista --parencite(galvan2014a)--. El paradigma de Popper para su propuesta, denominada
«racionalismo crítico», fueron las ciencias empíricas como la física. El racionalismo
crítico responde al problema de la inducción, al contrario del verificacionismo, 
al mismo tiempo que no cae en un escepticismo; en su lugar, busca que las teorías 
tengan que cotejarse con una base empírica --parencite(galvan2014a)--.

Las deficiencias de esta propuesta son perceptibles cuando se pretende aplicar a
ciencias que no resaltan por su capacidad de cotejo empírico, como son las
ciencias históricas. En este sentido, puede argumentarse que el paradigma popperiano
supone una subjetividad «libre de condicionamientos, que pretende aprehender la
estructura ontológica última del mundo» --parencite(galvan2014a)--.

Kuhn, otro filósofo de la ciencia y crítico de Popper, sostiene que parte de
este problema se debe a que la interpretación se percibe como algo distinto
a la experimentación cuando en realidad experimentar es interpretar. Esto lo sostiene
argumentando que los cambios en la ciencia no ocurren cuando se refutan
o verifican determinadas hipótesis, sino cuando sucede un cambio de paradigmas, los
cuales considera que son

> realizaciones científicas universalmente reconocidas que, durante cierto tiempo, 
> proporcionan modelos de problemas y soluciones a una comunidad científica --parencite(khun1969a)--.

\noindent Es decir, solo cuando un conjunto de hipótesis se vuelven insostenibles ante
los datos arrojados por la experimentación se buscan nuevas «visiones del mundo»
que ayuden a salir de semejante crisis --parencite(khun1969a)--. Cuando el científico 
muda de interpretación, 
incluso con los mismos medios y aparatos perceptivos, este da con distintos
resultados, que retroalimentan y se sustentan a partir del nuevo paradigma por el
que ha optado --parencite(garcia2008a)--, dando hincapié a argumentar que la experimentación 
es también una experiencia hermenéutica dadora de sentido.

Esta flexibilidad permite sustentar que, pese a la *total carencia de sentido* de 
este ejercicio mental para las ciencias empíricas —según el criterio
de verificabilidad o falsabilidad—, su hipótesis yace en otro 
terreno. Lo rescatable de semejante ejercicio no es aquello por corroborar 
o refutar, sino los «paradigmas» que alimentan o desestiman tanto la relación entre 
el creador y lo creado como la relación entre un objeto creado inalienable a su sujeto creador.

La tentativa sería cotejar la posibilidad de tratar este supuesto vínculo inalienable
como una «realización cultural» que durante cierto tiempo proporcionó un «modelo 
cultural» óptimo para la «visión del mundo» desde su contexto. Sin embargo, ante la paulatina
descentralización de la cultura, este modelo se presenta como conflictivo debido 
a que ya no da «soluciones» a las problemáticas culturales actuales sino que incluso 
las intensifica.

Ante la «crisis» de este modelo, quizá lo más pertinente es la búsqueda de un nuevo 
paradigma que más que tratarse de una refutación empírica puntual —como lo es la continua 
demostración de que la economía de la [PI]{.versalita} produce una desigualdad 
económica --parencite(cerlalc2015a)--— implique un cambio de «visión» respecto a la importancia del 
quehacer cultural junto con sus necesidades de reproducción y diversificación.

Si bien este uso heterodoxo de la propuesta kuhneana debe de afinarse, este
esquema de trabajo permitiría empezar a entender la [PI]{.versalita}

* desde un contexto principalmente histórico, donde incluso se considere
  tratar el concepto a partir de la historia de las ideas, ya que su génesis bebe
  directamente de las políticas y reformas económicas generadas a partir del siglo [XVI]{.versalita} —aunque
  la fundación del concepto se empieza a dar hasta mediados del siglo [XX]{.versalita}—, con el fin
  de aglutinar distintas doctrinas jurídicas en pos de una ampliación global de la
  economía generada por la [PI]{.versalita} --parencite(stengel2004a)--;
* como un término con un alto contenido ideológico, ya que en su ahistoricidad y
  universalidad reposa una visión del mundo muy particular, desde la cual el nexo entre 
  el creador y lo creado es una relación inalienable entre un sujeto creador 
  y un objeto creado que en un contexto político y económico beneficia principalmente
  a países desarrollados y al «capitalismo global», y
* a partir de una perspectiva filosófica, ya que su impacto en nuestra cultura
  no se explica de manera suficiente a partir de las ciencias histórica, política,
  social o económica, pues sus fundamentos residen en los escritos de filósofos modernos bajo
  una disciplina que en la actualidad se denomina «teoría de la [PI]{.versalita}».

\noindent De ser posible este tratamiento, también se abriría la tentativa de comprender
la [PI]{.versalita} —en lugar de justificarla— como anclada a un horizonte cultural
distinto al que se habita actualmente. Así, en lugar de hablar de «teoría de la
[PI]{.versalita}», se puede dialogar sobre las implicaciones históricas y filosóficas
de la [PI]{.versalita}…

---

\begin{flushleft}
Autor: Ramiro Santa Ana Anguiano. \\ Editora: Mariel Quirino Andrade. \\ Redactado: marzo del 2017. \\ Última modificación: noviembre del 2017. \\ Escrito bajo \href{https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre}{Licencia Editorial Abierta y Libre}. \\ Contenido disponible en \href{http://xxx.cliteratu.re/}{xxx.cliteratu.re}.
\end{flushleft}
