# La persona y el mundo como información

\noindent La persona no solo es carne, hueso, tiempo, juego, quimera o historia, 
también es información. Pero ¿qué es la información? Varios dicen que es «algo» 
abstracto, ahistórico e independiente del hombre, incluso que esta 
«*quiere* ser libre» --parencite(barlow2016)--. La información avanza sobre
su propio reino al mismo tiempo que se expresa a través de las épocas. Nadie
puede detenerla; nadie es su dueño. La información se manifiesta, se abstrae,
se concretiza y evoluciona según el actuar del hombre. La historia es solo
el camino para su realización; el mundo, el campo de observación. 

Sin embargo, en su incesante paso esta comienza a difuminarse si no hay sudor 
humano que la siga alimentando. Esta cesa su autorrealización cuando quienes la 
controlan la hacen ir por el sendero que dicta su amo: la «malinformación» o la
«desinformación». O bien, su muerte arriba al ser arrojada al fuego, al sucumbir
en lo que se supone le es inesencial: la historia del hombre. De nuevo —sin 
platonismos o hegelianismos—, ¿qué es la información? Quizá su relación con
los datos y los hechos permitan una aproximación.

\vskip 1em\noindent La posibilidad de identificar el carácter idealista de esta 
primera definición de la información se debe a que existen *datos* en las 
historias de la filosofía, en los diálogos platónicos o en la 
*Fenomenología del espíritu*. Solo basta *relacionarlos* para encontrar sus 
similitudes.

De primera mano el dato se nos ofrece como dado, como algo que yace ahí en un 
documento, en un objeto o en un conjunto de bits. Sin importar su origen o 
«naturaleza», el dato no es algo dado de suyo, sino en torno a las personas. La 
relación se da durante su constitución o creación, o cuando es interpretado o 
valorado. El dato, más que estar ahí de manera inerte e independiente al hombre, 
implica la actividad de un sujeto.

Supongamos que la existencia de un dato no precisa de la persona para su
constitución o su interpretación. Es decir, imaginemos que el dato seguiría
existiendo sin sujeto consciente y que su validez no se ve afectada por el
quehacer hermenéutico o axiológico. Por ejemplo, los datos que validan alguna 
clase de teoría científica.

Aún así tenemos que la subsistencia del dato depende de la capacidad técnica de
un sujeto para su conservación. Sin la cultura letrada y las ciencias que son
reproducidas o expandidas por las personas, el conocimiento científico caería en 
el olvido. Con la constante reinterpretación y análisis, la misma técnica 
permite la conservación o la destrucción de hipótesis científicas. Diversos 
datos sobre la física aristotélica han perdido su validez científica para pasar
a ser datos históricos. La quema de una archivo implica la pérdida de datos,
pero más específicamente, la capacidad técnica del sujeto para destruirlos.

El dato se presenta como una especie de unidad simbólica que de una u otra forma
implica la actividad de un sujeto para que esté a disposición de más personas o 
para ocultarla de manera intencional. El sujeto lo constituye en una unidad, lo 
mantiene, lo reconfigura según un marco teórico, paradigma o *episteme*, así 
como puede fabricarlo, falsificarlo, deteriorarlo o eliminarlo. En su 
constitución, interpretación o conservación el dato exige la actividad 
consciente de un sujeto. El dato implica una actividad teórica-práctica por el
cual lo subjetivo se integra en un proceso objetivo —a saber, el dato mismo—
--parencite(sanchez2003)--. En fin, al dato le es indisoluble la praxis humana.

Dicha indisolubilidad implica que la objetividad del dato muchas veces —sino es
que siempre— es supuesta. La suposición no es de índole epistemológica u 
ontológica, sino sociopolítica. La fiabilidad sobre la objetividad de un dato 
*en parte* depende en la confianza depositada en los sujetos encargados de 
constituirlo, interpretarlo o conservarlo. En este sentido, el dato se vuelve 
conflictivo o incluso es descartado si existen serias dudas sobre la actividad 
del sujeto que yace de fondo.

En la historiografía colimense existe una disputa sobre si la fundación de la
Villa de Colima se dio en dos ocasiones. En el análisis de los argumentos a favor
o en contra se encontró que en el uso de los mismos datos, se llegaron a 
conclusiones dispares. En la crítica de los métodos expuestos se da con que la 
gran controversia gira en torno a la valoración dada a un dato determinado: un 
párrafo de la *Relación Sumaria* de Lebrón de Quiñones. Ante semejante 
fragilidad teórica se opta por no validar y no dar por hecho una supuesta doble 
fundación.

La suspicacia llega a tal grado que ante la debilidad del dato no solo entra en 
juego su validez en sí mismo o en relación con algún sujeto, también entra en 
crisis el hecho que pretende fundar. La problemática de la fundación de la Villa 
de Colima se debe a que no existen datos concluyentes sobre su origen —su acta 
constitutiva está desaparecida. El dato ayuda a comprender lo que las cosas son
cuando ofrece una manera de pensar que evita una falsa totalidad: «Tu argumento
no es válido, faltan datos para corroborarlo». En este sentido sería sugestivo
repensar al dato como una especie de ícono descrito por Beuchot. El dato quizá
pueda comprenderse de un modo más cabal si se trata como un elemento particular
que va a la universalidad de algún hecho o conjunto de hechos, como algo que
va hasta lo más profundo e incluso pretende dar sentido a la vida 
--parencite(beuchot2012)--.

Aunque la mayoría de las ocasiones los datos son tratados con desdén en relación
con los hechos, en su ausencia un hecho permanece desconocido. Lo que acaece, lo 
irreversible, lo que preconfigura el devenir histórico y condiciona el quehacer 
científico afecta a las personas aún en la ignorancia de semejantes hechos. Sin 
embargo, la posibilidad de anclar un hecho en un conjunto más vasto que permita
encontrar un sentido o significado —constantemente reconfigurado y disputado—
depende de la cantidad y calidad de los datos. Un hecho es relevante en la
medida en que diversas unidades simbólicas son puestas en *relaciones* 
significativas para ciertos sujetos.

El hecho, aunque importante de suyo, crea lazos significativos en la vida de los
sujetos según su constante reinserción en la red significante que es la 
historia. Al decir de Zizek, los hechos pasados se presentan como una paradoja
del «falso reconocimiento» porque lo que «ya no cambia» es transformado a lo que
«siempre ha sido» por medio de la actividad de un sujeto 
--parencite(zizek2012)--. ¿Cómo es posible «cambiar» el pasado? ¿Qué hace que
el hecho, aunque irreversible, sea siempre reconfigurable? La relación del hecho
con la vida de un sujeto o su mundo está siempre mediada: al hecho no se accede
de manera directa, sino a través de los datos que permiten su intelección.

El dato como unidad simbólica dependiente de la actividad de algún sujeto tiene
la característica de darle disponibilidad temporal a un hecho más allá de su
mismo acaecer. Por el dato, el hecho trasciende su propia temporalidad y permite
su intelección aún después del acontecimiento. En la ausencia de datos, no hay 
modo fehaciente de corroborar los hechos. En la presencia de nuevos datos, es
posible reconfigurar lo irreversible. Y no menos interesante, en la 
falsificación de los datos se pueden gestar hechos cuyo único sustento es la
confianza depositada por cada sujeto en su supuesta efectividad.

La paradoja tiene sustento cuando el hecho desea percibirse con independencia de
los datos que lo sustentan. Sin embargo, más paradójico resulta la pretensión de
acceder a los hechos ignorando que su disposición está determinada por esos
terceros de confianza entre los hechos y los sujetos; a saber, los datos. El
hecho se presenta con un fuerte carácter fiduciario que deja abierto un 
problema epistemológico de fondo: la plasticidad del dato: la certidumbre basada
en unidades simbólicas dependientes de la actividad de ciertos sujetos.

Si los datos son unidades símbolicas, una especie de intermediarios entre los
hechos y los sujetos, los vínculos que unen a cada uno de los elementos son
relaciones de significado y sentido distintos a estos mismos. El dato o un
conjunto de datos permanecerían desperdigados sin los vectores que los unen
unos a otros o entre los hechos y los sujetos. Esta clase de nexos no se gestan
de manera arbitraria, sino que obedecen a la pretensión de encontrar cierta
ilación. De nueva cuenta, estos vínculos dependen de la actividad de un sujeto 
que busca dar cuenta de hechos —su significado y sentido— a través de un 
conjunto de datos. 

Estas conexiones también se distinguen de los datos y de los hechos porque no 
son el criterio de posibilidad para la intelección de los hechos, ni tienen la 
característica de ser irreversibles. Estas relaciones, por su carácter dinámico
no son esto *o* aquello, sino lo que facultad hablar de esto *con* aquello.

Gracias a estos vínculos es posible constituir una red significativa de hechos 
que al mismo tiempo facultan al sujeto situarse en su circunstancia. La vida se
presenta como un quehacer donde la persona se hace a sí misma 
--parencite(ortega2007)-- a través de la serie de nexos que le permiten
reconfigurarse a cada instante por medio de esta «dialéctica de las 
experiencias» por la cual «va siendo y des-siendo; es decir, viviendo»
--parencite(ortega2007)--.

La información puede percibirse con este fuerte carácter histórico y contextual.
La información sería precisamente estos vínculos significativos entre unidades
simbólicas, hechos y sujetos. El modo de ser de la información se presenta
como problemático porque en varios casos implica una diversidad de sentidos 
--parencite(briggle2009)-- no reducibles a un objeto. Tal vez sea porque la 
información no es en sí un objeto, sino una relación objetivable. Entre los
distintos sentidos puede decirse que la información es *sobre*, *para*, 
*a modo de* o *en* algo --parencite(briggle2009)-- porque siempre crea una
conexión entre datos, hechos y sujetos, al mismo tiempo que se gesta como 
objeto en el plano intelectivo o al momento en que estas relaciones crean nuevos
datos.

El carácter siempre variable evita que la información se manifieste de un modo 
solo mecanicista. La dinamicidad de la información no es reducible a un 
movimiento pendular. Si el péndulo es el sinónimo de una ciencia y filosofía 
mecanicista, y su contraparte es la espiral --parencite(xirau1994)--, los 
vínculos significativos son un bucle que entre más cercano esté a un supuesto 
núcleo, los nexos van siendo sustituidos por otros más específicos y sutiles, 
hasta generar redes de significación cada vez más complejas y variadas.

La información se presenta como la posibilidad misma de un mundo cada vez más 
variado y rico en sentidos y significados. La información no sería nada etéreo y 
ajeno a la voluntad de los hombres, sino la condición que permite armar una 
trama entre datos, hechos y sujetos. La información sería el supuesto mínimo que 
permite a las personas su sustento ontológico diario o su cuestionamiento 
filosófico de fondo…

\vskip 1em\noindent Que la información contemple un carácter histórico, 
contextual y dependiente de la praxis humana quiere decir que su neutralidad
puede ser entredicha. La información se torna en cuestión política cuando los
vínculos que la constituyen dependen de marcos jurídicos y geográficos 
determinados.

Las nociones de la información como una clase perteneciente al reino de las 
ideas que espera su expresión concreta —lo que también se conoce como 
«propiedad intelectual»— o como un conjunto de bits en el ciberespacio 
invisibilizan dos hechos. La información en código binario no está suspendida 
sobre una nada tecnológica como las «nubes» o el internet. Más bien, la 
información yace sobre un conjunto de nodos físicos —los servidores— y cables de 
fibra de vidrio que constituyen la estructura física del internet. En varios 
casos, las políticas de acceso y de uso de la información que pasa a través de 
cada uno de los nodos y fibras son poco claras, ya que su mantenimiento es 
realizado por proveedores de internet privados. La supuesta autorrealización y 
tendencia a la libertad de la información queda suprimida por la capacidad de 
análisis de datos y su comercialización por parte de estos proveedores. En este 
sentido, no es fortuito que la «neutralidad en la red» se considere un tema de 
privacidad y seguridad de los usuarios, y también como una cuestión que afecta 
directamente a comunidades y culturas.

Por otro lado, la información como perteneciente a un Mundo de las Ideas deja
de lado que su «expresión concreta» se descifra a modo de un derecho de 
monopolio garantizado por algún Estado. La información por la cual se produce
un descubrimiento, una obra, un tratado comercial, una denominación de origen,
etcétera, evidencia su carácter dependiente al «mundo concreto» al permitirse
su manipulación exclusiva por parte de su «creador». Los derechos de la 
propiedad intelectual manifiestan que de manera efectiva la información está
subsumida a los sujetos, sus posibilidades o límites de realización, y, por
supuesto, sus propios intereses.

Lo que esto quiere decir es que la infraestructura de la información —lo que
posibilita la creación, la manutención o la expansión de nexos entre datos, 
hechos y sujetos— va a tono a la realidad política y jurídica del mundo en una 
especie de mutua subordinación. Por una parte las diferentes regulaciones 
estatales e internacionales limitan el potencial de la información para minar 
los fundamentos políticos, económicos y sociales. Por ejemplo, la propiedad 
intelectual como punto de control entre cómo se quiere gestionar una creación y 
lo que tecnológicamente ya es posible hacer; las iniciativas de ley para regular 
o prohibir el comercio con criptomonedas, cuya estructura tecnológica deja de 
fuera la posibilidad de centralización monetaria como es común en las monedas 
fiduciarias, o los distintos acuerdos internacionales o nacionales que regulan 
el acceso a la información, principalmente si esta es de interés social.

Pero la infraestructura de la información también modifica el aparato 
tecnológico de los diversos mecanismos de poder nacionales o internacionales.
La inteligencia artificial, la criptografía y la automatización de procesos así
como causan un enorme recelo, también están siendo aplicadas en distintos 
aspectos funcionales que dan sustento a un sin fin de instituciones. Esta clase
de tecnologías permiten aumentar la eficiencia y eficacia en la gestión y la
centralización del poder, al mismo tiempo que permiten la ofuscación de su
funcionamiento al traducirse ya no solo en un lenguaje jurídico, sino también
en código y en algoritmos: el código es también ley --parencite(lessig2009)--.

La infraestructura del mundo se tecnifica y la infraestructura de la información
se politiza a tal punto donde una y otra empiezan a conformar una sola 
infraestructura. El crecimiento de vínculos en este ecosistema se traduce en
una estandarización sobre el trato tecnológico de la información que entra, sale
o se produce en cada uno de los sistemas estatales e internacionales.

En este sentido, el mundo, más que un mundo de la vida, es un sistema-mundo 
moderno --parencite(wallerstein2007)--, en donde existen ciclos de trabajo para
su reproducción y expansión mediante la centralización de procesos para el
cumplimiento de objetivos —ideológicos— específicos. La neutralidad de la 
información ya no solo queda entredicha en su infraestructura —la organización 
en los modos de producción para generar la red de significaciones—, sino también 
en su modo de efectuarse.

El procesamiento de la información ha dependido en la capacidad de las personas
en generar conexiones cada vez más amplias, complejas y sutiles, así como en
el ingenio de cubrir estos vínculos si así parece conveniente para sus 
intereses. Sin embargo, en el sistema-mundo la tarea de creación de conexiones
se busca automatizar y constituirse a través del análisis de grandes cantidades
de datos mediante inteligencia artificial. Además, su ofuscación también se
vuelve cada vez más una tarea realizada por una máquina. La fiabilidad que se
le da a los sistemas informáticos es proporcional a los límites humanos en 
procesar de manera eficiente la información o en su capacidad de cifrarla. 

La carrera criptográfica y en torno a la relación artificial por parte de 
gobiernos o corporaciones obedece a los intereses comerciales y de seguridad 
nacional implicados en las conexiones posibles de un mar de datos. Estas 
unidades simbólicas tienen su origen en la actividad de los usuarios en el 
sistema-mundo ya por defecto: el internet.

Por ello, la suspicacia ante la neutralidad de la información es porque su 
infraestructura por excelencia está al alcance de casi todos, pero la capacidad 
de procesamiento de la información generada por el usuario y los intereses 
políticos o económicos detrás de ellos no son accesibles públicamente. Lo que 
puede ser irrelevante para un usuario, como los términos de búsqueda, para las 
corporaciones pueden ser una serie de conexiones que permiten publicitar un 
producto de una mejor manera, o para los gobiernos, la constitución de vínculos 
delictivos, como la piratería o el terrorismo.

\vskip 1em\noindent La información como parte de un sistema-mundo moderno y 
carente de neutralidad es una de las realidad posibles que en la actualidad se
presenta como la hegemónica. El control sobre estos vínculos de unidades 
simbólicas, hechos o sujetos supone que la red significativa que forma nuestro 
mundo es un objeto sujeto a propiedad.

Los sistemas de propiedad intelectual tienen el efecto inmediato de ejercer el
control sobre sus diversas manifestaciones. Sin embargo, su sustento jurídico
y filosófico dependen de lo que se considere que es la información. Cuando la
información es inmaterial y ciudadana de otro reino, su derecho de control es
traducido como «expresión concreta de una idea». O bien, si la información es
una red de nexos significativos, su dueño es quien posee las bases para su 
reproducción o las tecnologías más óptimas para un alto grado de procesamiento.

Pero la información es primordialmente trabajo: es praxis humana. La información
puede ser praxis reiterativa y burocratizada --parencite(sanchez2003)-- que 
convierte a las unidades simbólicas —gestadas y mantenidas por las personas— en
redes afines al sostén del sistema-mundo moderno. Pero la información también
es posible manifestarse como praxis creativa que hace frente a nuevas 
necesidades prácticas y teóricas en un mundo cada vez más humano, hasta el grado
de poder crear nuevos regímenes sociales, económicos o políticos 
--parencite(sanchez2003)--.

La información como praxis creativa es perceptible en dos grandes movimientos
antagónicos al uso propietario de la información: el *software* libre y el
código abierto. En el *software* libre se manifiesta explícitamente la 
relevancia social y política que tiene el desarrollo de *software* a tal grado
que la información ha de permanecer siempre «libre» --parencite(stallman2016)--. 
En las comunidades de código abierto, su modelo de trabajo y de toma de 
decisiones pretende evitar la centralización del poder mediante un modo de 
producción horizontal que ha sido llamado «modelo bazar» 
--parencite(raymond2016)--.

En ambos casos se asiente en que la información, más que una clase de organismo
abstracto, es un fruto cultivado por una comunidad, que, debido a su valor, 
nadie *debería de* apropiárselo. Aunque el carácter deóntico es más patente en 
el *software* libre, en la iniciativa del código abierto se vislumbra como el 
compromiso implícito que el programador tiene ante la comunidad. En cualquiera
de los casos, se busca que la información sea abierta, fuera del control de
cualquier tipo de corporación o gobierno. Para muchos, esto significa una
guerrilla abierta ante el carácter hegemónico de la información 
--parencite(swartz2008)--.

\vskip 1em\noindent Que la información sea o no un vínculo significativo a 
servicio o no del sistema-mundo, que el dato sea o no una unidad simbólica o que
el hecho sea o no el acaecer irreversible pero reconfigurable dependen del trato
filosófico que se le dé a cada uno de estos conceptos. Como es patente, este
trabajo no es lo suficientemente riguroso para cumplir con semejantes
expectativas.

Con mucha probabilidad este escrito se presenta más a debate y sospecha, que a
un conjunto ordenado de verdades. Pero sin importar la cantidad de datos que
soporten estos argumentos, la exigencia que yace de fondo es la urgencia de
tratar a la información como categoría filosófica. Como varias veces lo ha
reiterado Floridi, el quehacer filosófico necesita de una vez por todas indagar
sobre lo que es la información, para así buscar otros puertos que permitan la
comprensión filosófica de fenómenos tecnológicos actuales 
--parencite(floridi2002)--. Para el caso de la propiedad intelectual, se parte 
del supuesto que su relación con la información permitiría dilucidar los
«fundamentos filosóficos» que le dan cabida y que el quehacer filosófico tanto
puede criticarlo como reproducirlo.

---

\begin{flushleft}
Autor: Ramiro Santa Ana Anguiano. \\ Redactado: diciembre del 2017. \\ Última modificación: diciembre del 2017. \\ Escrito bajo \href{https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre}{Licencia Editorial Abierta y Libre}. \\ Contenido disponible en \href{http://xxx.cliteratu.re/}{xxx.cliteratu.re}.
\end{flushleft}
