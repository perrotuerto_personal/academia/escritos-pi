# ¿Hegel contra Hegel?

\vskip -3em

## Propiedad intelectual y ¿colonialismo?

\vskip 3em

\noindent Del 2000 al 2016 la economía de la propiedad intelectual
([PI]{.versalita}) ha crecido un 421% --parencite(bm2018a,bm2018b)--. 
Si se hace una comparación desde 1990, esta ha tenido un alza del 
1,404% --parencite(bm2018a,bm2018b)--. 

Uno de los motivos de este fuerte crecimiento es el desarrollo 
tecnológico a nivel global. Desde los derechos de autor, pasando por
marcas, diseños industriales, secretos comerciales, patentes hasta
denominaciones de origen e incluso organismos genéticamente modificados,
«atributos de la personalidad» --parencite(fisher2016a)-- 
o algoritmos --parencite(drahos1996a)--, la [PI]{.versalita} es la 
médula espinal de lo que también se conoce como «economía del conocimiento». 
Este tipo de economía se caracteriza por su presencia en todos los 
sectores económicos cuyo objetivo es *la administración* del flujo de 
la información, como es la transferencia tecnológica o del conocimiento.

La relevancia económica de la [PI]{.versalita} ha inducido a la creación
de diversos tratados internacionales y la firma de acuerdos para tener 
reglas comunes de operación entre Estados. Pero también ha desatado un 
fuerte debate debido a que las modificaciones de leyes nacionales e 
internacionales en pos de la [PI]{.versalita} se han caracterizado como 
un «reforzamiento»; es decir, como un aumento en los años en los que 
alguna entidad —institución pública o privada, o individuos— tiene 
todos los derechos sobre cierta creación, también llama «expresión
concreta de una idea» --parencite(moore2014a)--.

La justificación de este «reforzamiento» de los derechos de la [PI]{.versalita},
o al menos de su viabilidad —aunque sea más laxa en comparación a la 
forma actual de estos derechos—, se ha catalogado como la búsqueda de 
una «teoría de la propiedad intelectual». El supuesto de esta indagación
es que con una teoría consistente de la [PI]{.versalita} es posible 
defender la necesidad de la constitución de una serie de derechos que 
la protejan.

Los principales proponentes de semejante posibilidad vienen del ámbito
académico anglosajón, principalmente de los campos de la economía y el
derecho, pero también de la filosofía --parencite(hughes1988a,moore2008a,shiffrin2007a,stengel2004a,drahos1996a)--.
Entre los autores clave para esta teoría se tiene a Locke y a Hegel.

A partir de la «teoría del trabajo» de Locke se justifica que la [PI]{.versalita}
ha de ser objeto de protección estatal ya que es el fruto obtenido por
el esfuerzo al momento de llevar a cabo una labor --parencite(locke2006a)--. 
Aunque en un primer momento el argumento parezca comprensible, este se 
presta a diversos problemas al intertarlo adaptarlo a la «naturaleza» 
de la [PI]{.versalita}. Si bien en Locke está presente este argumento, 
su intención original era la defensa de la propiedad privada, específicamente
la posesión de la tierra para la producción de bienes. A diferencia de
este tipo de propiedad, la [PI]{.versalita} no contempla un principio
de escasibilidad ni de exclusividad e incluso tampoco requiere de un
continuo mantenimiento.

Como Hughes —uno de los proponentes más citados en la literatura en torno
a la teoría de la [PI]{.versalita}— lo ha admitido, la justificación
lockeana de la [PI]{.versalita} aunque fértil, es muy engorrosa para
el mundo actual --parencite(hughes1988a)--. Por este motivo, diversos
académicos han centrado su atención en Hegel y en lo que se ha denominado
como «teoría de la personalidad». En síntesis, esta teoría indica que
la [PI]{.versalita} ha de ser objeto de protección estatal ya que es
la expresión de la voluntad del individuo: la manifestación de su
personalidad. A partir de ahí no solo queda justificada la [PI]{.versalita}
y sus supuestos consecuentes derechos, sino también la doctrina de los
«derechos morales» —el reconocimiento de la paternidad de una obra,
por la cual el autor puede negar su modificación o derivación por
parte de terceros, pese a que estos la hayan adquirido—.

Pero ¿cómo es que se articula esta teoría? Y principalmente, ¿es posible
ver en Hegel un defensor de los derechos de la [PI]{.versalita} o al menos
de la existencia pertinente de la [PI]{.versalita}? ¿Qué hay detrás de
todo este asunto?

\vskip 1em\noindent El análisis de Hughes en «The Philosophy of 
Intellectual Property» es un texto citado con mucha frecuencia debido
a que sintetiza varios de los argumentos que se han hecho en la búsqueda
de una teoría de la [PI]{.versalita}. El caso de la teoría de la
personalidad de Hegel no es la excepción.

En este tipo de teoría, la [PI]{.versalita} se piensa como 
autorrealización o actualización del yo, expresión de la voluntad del
individuo o de la persona, y como dignificación o reconocimiento del
autor --parencite(hughes1988a)--. Para conseguirlo es necesario el
control sobre los recursos externos; a saber, que el Estado garantice
al individuo los medios para la gestación de su personalidad 
--parencite(hughes1988a)--. Estos medios resultan ser las posibilidades
económicas desprendidas de la [PI]{.versalita} a través de la venta
o renta de su obra: *el control* de la creación como una forma en la
cual el creador tiene la posibilidad de expresarse como un individuo
distinto a los demás. Lo que está en la base de este vínculo entre
el autor y su obra es la asunción de que la idea es propiedad del
creador porque es su manifestación --parencite(hughes1988a)--.

De acuerdo con Hughes, Hegel centra su teoría de la propiedad en la
libertad del individuo, en donde de primera mano es posible detectar
dos tipos de propiedad --parencite(hughes1988a)--. La primera es la
propiedad «interna» que es el propio yo y la voluntad; la segunda, la
«externa»: los objetos con posibilidad de alienación --parencite(hughes1988a)--.
Esto implica una oposición del sujeto con el mundo donde la apropiación
surge como un modo de imponer el yo. De ahí la relevancia de la propiedad,
ya que esta se convierte en una expresión de la voluntad: permite la
búsqueda de la libertad --parencite(hughes1988a)--. En este sentido,
los derechos de la propiedad en sí no provocan esta libertad, sino solo 
garantizan que esta suceda ya que evade la posible vorágine entre 
diversos individuos que anhelan imponer su propio yo --parencite(hughes1988a)--.

El traslado de la posesión a la propiedad se da cuando existe una
aceptación social de los reclamos hechos por un individuo. Es decir,
la propiedad es la constitución de la objetividad en la posesión de un
objeto, la cual se mantiene siempre y cuando la voluntad se manifieste 
sobre este --parencite(hughes1988a)--. Dos características de esta
posesión objetiva es que el trabajo es condición suficiente mas no
necesaria; a diferencia de Locke, la labor llevada a cabo no es un
requisito necesario, ya que bien puede una voluntad expresarse sobre
un objeto sin necesidad de haberlo elaborado o cuando ya el trabajo
ha terminado --parencite(hughes1988a)--. No solo el trabajo es 
innecesario para la apropiación, sino que también es posible ocupar un 
objeto sin darle utilidad alguna, ya que la voluntad puede expresarse 
sin hacer uso de los objetos que se adjudica, ya que bien pueden 
considerarse como una extensión de su propio yo --parencite(hughes1988a)--.

La posesión de un objeto puede darse al menos de tres maneras: la
ocupación física, la imposición de una forma o imponiéndole una marca
--parencite(hughes1988a)--. Aunque esto da mucha apertura, bien sea
por ser demasiado subjetiva o artificial, también permite la entrada
de un mecanismo que dé posibilidades equitativas de posesión: negar
la apropiación a *x* persona si va en detrimento a las posibilidades de 
posesión de *y* persona --parencite(hughes1988a)--. Con esto se hace
patente que la objetividad de la posesión no solo es un acto subjetivo
de apropiación, sino también un reconocimiento social y estatal de este
acto, siempre y cuando esto no afecte a la comunidad de individuos.

Como puede observarse, esta serie de argumentos son sobre lo que Hegel
pensó sobre la propiedad. Para el caso específico de la [PI]{.versalita}
este filósofo no ve la necesidad de justificarla en analogía a la
propiedad física --parencite(hughes1988a)--. Sin embargo, no dice nada
relevante más allá de su importancia para el desarrollo de las ciencias
y las artes --parencite(hughes1988a)--.

Además, acorde a Hughes esta justificación se presta a diversas
complicaciones. En un primer momento, la personalidad se manifiesta
en una relación entre el sujeto y sus objetos, pero se dan casos en
donde la expresión de un creador sobre su creación no es de fácil
reconocimiento para el resto de los individuos --parencite(hughes1988a)--.
En no pocas ocasiones la [PI]{.versalita} carece de rasgos distintivos 
de personalidad que su creador subestima o su identificación solo es 
perceptible para los expertos en el campo en el cual se gesta esta 
propiedad --parencite(hughes1988a)--. Este problema de identificación 
en la externalización puede acarrear que la protección jurídica de la 
[PI]{.versalita} sea relativa al grado de expresión personal que esta 
contenga.

Pero para Hughes este no es el problema más grave: la teoría de la
personalidad de Hegel es paradójica --parencite(hughes1988a)--. Para 
evidenciarlo, se puede decir que:

1. El propietario actual ve en su propiedad una expresión de su 
   personalidad.
2. El Estado garantiza protección de la creación ya que esta se 
   identifica con su creador.
3. El individuo aliena su propiedad a cambio de otros bienes que le 
   permitirán continuar con la gestación de su personalidad.
4. Con el acto de alienación el autor hace patente que no existe un
   vínculo intrínseco entre él y su obra: manifiesta que ese objeto
   ya no forma parte de la expresión de su personalidad.
5. El Estado ya no puede garantizar protección, porque ya no existe
   identificación.

\noindent Tal como se justifican los derechos de la [PI]{.versalita} con la 
teoría de la personalidad, la alienación hace imposible que el creador 
tenga control sobre su creación una vez alienada: una característica 
fundamental de los derechos de la [PI]{.versalita}; a saber, *el control* 
sobre la creación, lo que Hughes llama «la determinación del futuro» 
del objeto e incluso del resto de los individuos que lo adquieren --parencite(hughes1988a)--. 
Además, según Hughes, para Hegel esta completa alienación de la [PI]{.versalita}
es semejante a la esclavitud o al suicidio: elimina el aspecto 
universal del yo --parencite(hughes1988a)--.

Una solución de este problema es hacer de la alienación algo parcial.
En lugar de una completa disgregación del sujeto de su objeto, la
alienación parcial implica un reconocimiento de la paternidad que
faculta al autor cierta protección ante los usos desaprobados que
vayan en detrimento de su personalidad --parencite(hughes1988a)--.
De ahí es como surge la doctrina de los derechos morales, muy común
en las tradiciones jurídicas no anglosajonas y que complementan a la
doctrina del *copyright* del derecho anglosajón, ya que permite 
restricciones en el uso que los derechos patrimoniales —derivados en
gran parte de la teoría del trabajo de Locke— en sí mismos no pueden
contener.

\vskip 1em\noindent Pese a las dificultades que desatan las teorías del 
trabajo o de la personalidad de Locke y de Hegel, sin mención explícita
existe un optimismo en cuanto a su pertinencia para la constitución de
una teoría que tanto justifique a la [PI]{.versalita} como también
dé pauta para su protección jurídica. Pero no todos los académicos
comparten este entusiasmo, como se puede observar en la crítica que
hace Schroeder sobre la supuesta «teoría de la personalidad de Hegel».

Para esta académica semejante teoría es más bien un refugio para muchos 
de los defensores de la [PI]{.versalita} ante diversas objeciones --parencite(schroeder2004a)--:

* En contra de los utilitaristas da una opción al argumento naturalista
  de la [PI]{.versalita} desprendida de Locke.
* En contra quienes no consideran a la [PI]{.versalita} como una forma
  de propiedad ofrece argumentos para demostrar lo contrario.
* En contra de los que no les parece relevante la [PI]{.versalita}
  otorga una justificación sobre su importancia en relación con otros
  tipos de propiedad.

\noindent Pero no solo eso, acorde a Schroeder por lo general son equívocas
las citas de Hegel de las que se valen estos defensores, incluso al 
punto de ir en contra del proyecto hegeliano --parencite(schroeder2004a)--.
Para el sistema hegeliano es inconcebible que las leyes y los derechos
se den en la naturaleza; en su lugar, son construcciones que permiten
un escape de lo natural en pos de la libertad --parencite(schroeder2004a)--.
La justificación de la propiedad va a tono con las funciones que tiene
el Estado moderno para garantizar esta libertad --parencite(schroeder2004a)--.

Según la interpretación de Schroeder, la propiedad es uno de los primeros
pasos para pasar de la concepción del individuo abstracto de la tradición
liberal e ilustrada del siglo [XVII]{.versalita} al ciudadano concreto
del Estado moderno que pretende Hegel --parencite(schroeder2004a)--. 
Esto hace que la propiedad efectivamente se centre en la persona, pero 
principalmente en su espectro político. En este campo, la propiedad no 
se relaciona directamente con la ciudadanía, sino con uno de los 
requisitos para su obtención que Schroeder llama «subjetividad legal» 
--parencite(schroeder2004a)--.

La subjetividad legal es la capacidad de los individuos de obedecer
las leyes --parencite(schroeder2004a)--. Esto es el fundamento necesario
para la constitución del Estado moderno que, a diferencia del Estado
feudal, no se trata de un «gobierno de los hombres» sino un «gobierno
de las leyes» --parencite(schroeder2004a)--. Este estado de derecho
hace reconocer a otros como límite de los derechos de un individuo --parencite(schroeder2004a)--.
Esta subjetividad es condición necesaria mas no suficiente, ya que es
abstracta, solo es la forma de la personalidad cuyo contenido es la
ética y la moral --parencite(schroeder2004a)--.

En este sentido, los derechos de propiedad no existen porque se consideren
correctos, justos o bueno en sí, sino porque funcionan como un medio 
para la consecución de fines --parencite(schroeder2004a)--. Esto 
también hace explícito que la propiedad no es una relación natural entre
el sujeto y sus objetos apropiados, sino un elemento inicial y 
artificial que salvaguardará al futuro ciudadano en un Esto moderno
--parencite(schroeder2004a)--. Esto es más radical de lo que varios
teóricos de la [PI]{.versalita} pensarían. La propiedad no es una
característica que *se da* en la comunidad de individuos. En su lugar,
la propiedad *se construye* ya que es fundamental para la fundación del
Estado moderno: sin propiedad no hay Estado.

Para Schroeder esto es contraintuitivo para muchos de los teóricos de
la [PI]{.versalita} porque implica una concepción de la libertad muy
distinta a la tradición liberal del mundo anglosajón al que están
acostumbrados --parencite(schroeder2004a)--. En esta tradición la 
libertad existe en un estado natural: «todos los hombres nacen libres». 
Bajo esta noción acontece la libertad de apropiación; es decir, la 
propiedad se da en un estado natural por cual el Estado solo se encarga 
de reconocerla --parencite(schroeder2004a)--. Locke es un buen ejemplo, 
ya que parte del estado de la naturaleza e incluso del otorgamiento 
divino del mundo a Adán y Eva para justificar la propiedad --parencite(locke2006a)--.

Hegel está al tanto de esta noción de la libertad, pero se distancia de
la tradición liberal de la que parte para argumentar que no es posible
la libertad en la naturaleza, ya que esta solo es limitada, negativa y 
abstracta --parencite(schroeder2004a)--. En su lugar, existe la necesidad 
de leyes positivas y concretas que permitan la libertad en un contexto
tan artificial como es el Estado --parencite(schroeder2004a)--. Tan 
consciente es Hegel del carácter paradójico de la libertad en la tradición
liberal, que él mismo la identifica en la «tercera antinomia» señalada
por Kant y la cual es uno de los puntos de partida para su análisis
de la propiedad --parencite(schroeder2004a)--.

Acorde a la interpretación de Hegel realizada por Schroeder, para salir 
del estado de la naturaleza es necesario buscar el reconocimiento de 
otros, en donde el reconocimiento de la propiedad es un punto de partida 
--parencite(schroeder2004a)--.Esto implica que a toda propiedad le 
corresponde un objeto —el sujeto se reconoce por medio de sus 
posesiones—, pero también que este objeto no es «natural», sino un medio 
que sirve para la constitución de la subjetividad en un contexto 
intersubjetivo y artificial --parencite(schroeder2004a)--.También tiene 
como consecuencia que la tangibilidad del objeto no es necesaria: la 
propiedad no está delimitada por su estado físico, sino solo como un 
medio para la consecución de fines --parencite(schroeder2004a)--. Para 
el caso de la [PI]{.versalita}, esto quiere decir que esta no solo es 
un tipo de propiedad, sino tal vez es la más afín ya que su falta de 
tangibilidad permite comprenderla con más facilidad como un medio --parencite(schroeder2004a)--.

En este punto, la propiedad es una formalidad: el contenedor de la
subjetividad, la característica que permite delimitar a un sujeto del
otro --parencite(schroeder2004a)--. Además, los requisitos de la
propiedad son la posesión, el goce y la alienación --parencite(schroeder2004a)--.
Esto no se debe a que la propiedad tenga algún tipo de naturaleza o
esencia, sino porque son características funcionales que permiten la
creación de la subjetividad --parencite(schroeder2004a)--.

La posesión es la identificación de un objeto con un individuo: diferencia
al poseedor del no-poseedor, es decir, es un criterio de exclusión --parencite(schroeder2004a)--.
Esto hace patente que lo importante no es el objeto en sí, sino lo que 
este puede llegar a significar en relación a un sujeto --parencite(schroeder2004a)--.
De paso esto resuelve el problema de que la [PI]{.versalita}, a diferencia
de la propiedad, puede ser poseída por un gran número de individuos:
lo primordial no es quién puede poseerla exclusivamente, sino quién 
puede identificarse con ella --parencite(schroeder2004a)--.

El goce es el uso por el cual el sujeto permite distinguirse del objeto
identificado --parencite(schroeder2004a)--. Mediante el goce el sujeto
hace patente que él es el fin y el objeto un medio para su fin: es la
negación del objeto «en sí», de este como contenido, para pasar a ser
un medio, una parte de la forma del sujeto --parencite(schroeder2004a)--.
Esto también resuelve la dificultad de que la [PI]{.versalita} no puede
considerarse como tal ya que no comparte la característica de escasibilidad
del resto de tipos de propiedad. Lo relevante de la propiedad no es
quién puede tenerla o qué tanto hay para su apropiación, sino quién 
puede gozarla y, principalmente, quién puede controlarla --parencite(schroeder2004a)--.

Por último, la alienación permite al sujeto evitar la dependencia al
objeto en goce --parencite(schroeder2004a)--. Schroeder es muy enfática
al indicar que un error de Hughes es pensar que el consumo es la forma
por excelencia del goce y el abandono la forma principal de alienación
--parencite(schroeder2004a)--. Para esta autora el goce no tiene que
ser la completa aniquilación del objeto que implica el consumo, así
como la forma más relevante de alienación es el contrato --parencite(schroeder2004a)--.

A través del contrato un objeto es transferido a otro sujeto a cambio
de otro objeto --parencite(schroeder2004a)--. De nueva cuenta esto
da a entender que la tangibilidad no es la característica que interesa
del objeto, sino su función como medio que a su vez evidencia el 
reconocimiento mutuo entre subjetividades --parencite(schroeder2004a)--.
El contrato no solo se manifiesta un intercambio, también explicita que 
ambas partes se reconocen como sujetos.

Este tipo de reconocimiento intersubjetivo a través del intercambio
de la propiedad permite resolver una paradoja común en la idea de que
todo sujeto es un fin en sí mismo --parencite(schroeder2004a)--. Esta
paradoja tiene la siguiente forma:

1. Todo individuo es un fin en sí mismo.
2. Un individuo busca alcanzar su propio fin.
3. La consecución de fines requiere la ayuda de otros individuos.
4. Si un individuo sirve de medio para la consecución de un fin de
   otro individuo, quiere decir que este no es un fin en sí mismo.
5. Entonces no todo individuo es un fin en sí mismo.

\noindent Según Schroeder, para Hegel los individuos siguen siendo
fines en sí mismos en un Estado moderno ya que son los objetos los
que median en estas relaciones --parencite(schroeder2004a)--. La
propiedad es fundamental para el Estado moderno porque permite ser el
medio entre distintos fines, evitando la vorágine del «gobierno de los
hombres» --parencite(schroeder2004a)--. A diferencia de esta académica,
para Hughes no hay solución satisfactoria en Hegel, haciendo necesaria
la constitución de los derechos morales que eviten que la teoría de
la personalidad caiga por su propio peso --parencite(schroeder2004a)--.
Sin embargo, Schroeder indica que el origen de esta postura es una
errónea interpretación de Hegel, específicamente en el supuesto que
para Hegel el abandono es la forma fundamental de alienación
--parencite(schroeder2004a)--.

En este sentido, los derechos morales dan una solución innecesaria e
incluso contraria al sistema hegeliano --parencite(schroeder2004a)--.
Con mayor radicalidad, para Schroeder los derechos morales son fruto 
de un romanticismo que supone un supuesto vínculo íntimo entre el autor 
y su obra que confunde las relaciones abstractas de propiedad de Hegel 
con contenidos concretos de expresiones de la personalidad --parencite(schroeder2004a)--. 
Con esto queda que en Hegel no pueden justificarse los derechos morales, 
así como sí existe una teoría consistente de la propiedad al menos con 
el mismo sistema hegeliano o su noción de Estado moderno --parencite(schroeder2004a)--.

Aquí es donde Schroeder inicia su crítica al supuesto de esta teoría:
que de la justificación de la propiedad, más específicamente de la
[PI]{.versalita}, es posible argumentar la necesidad de una protección
jurídica.

Según la interpretación de esta autora, Hegel efectivamente indica que
la propiedad es necesaria para la libertad, así como la [PI]{.versalita}
es una forma de propiedad. Sin embargo, Hegel jamás menciona la necesidad
de establecer derechos de la [PI]{.versalita} --parencite(schroeder2004a)--.
Dentro de su sistema, para Hegel algunos derechos de propiedad son necesarios 
para la fundación del Estado moderno, pero no hay necesidad lógica de
derivar derechos de [PI]{.versalita}. A lo sumo puede existir un
error en la sociedad civil cuando un sujeto cree ser titular de una
propiedad que no le pertenece --parencite(schroeder2004a)--, pero esto
no justifica la creación de leyes para validar la apropiación intelectual.

Estas conclusiones parecen contraintuitivas solo si se asiente con los
siguientes dos supuestos. El primero es considerar que las creaciones
artísticas son únicas y personales, y como Hegel habla de la propiedad
como punto de partida para la constitución de la personalidad, entonces
el análisis hegeliano *debe de* orientarse a la protección de la [PI]{.versalita}
--parencite(schroeder2004a)--. El otro es pensar que como Hegel trabaja
el tema de la propiedad y los derechos de autor son el único tipo de 
propiedad que comenta de manera extensa, entonces el análisis hegeliano
*tiene que* orientarse a la protección de los derechos de autor y,
con ello, a los derechos morales --parencite(schroeder2004a)--.

Pero lo cierto es que ambos supuestos tienen su origen en esta postura
romántica sobre la relación del autor y su obra que no solo es imperceptible
en Hegel, sino que incluso es un punto de visto inverso a su sistema
que este mismo rechaza --parencite(schroeder2004a)--. Si Hegel le dedica
tiempo a los derechos de autor no es porque haya sido de su interés,
sino debido a que para Kant y Fichte consideran que son únicos y merecen 
un trato especial, opinión que Hegel no comparte e intenta demostrar 
--parencite(schroeder2004a)--. Por otro lado, para Hegel *los frutos* 
del trabajo artístico no son esenciales para la constitución de la 
personalidad --parencite(schroeder2004a)--. En Hegel la relación entre 
el autor y la creación solo se vuelve real cuando es externalizada: 
cuando acontece la alienación de un objeto para dársela a otro sujeto 
--parencite(schroeder2004a)--.

Bajo esta teoría de la propiedad, los derechos de la [PI]{.versalita}
darían a entender que estos objetos forman parte del sujeto no ya
como medio, sino como un fin, característica contraria a lo que Hegel
entiende por objeto --parencite(schroeder2004a)--. Además, la insistencia
de mantener un lazo entre el sujeto y el objeto, como pretenden los
derechos morales o de la [PI]{.versalita}, no solo traerían dificultades
para la alienación que acontece en un contrato, sino que también sería 
un indicativo de fetichismo --parencite(schroeder2004a)--.

Por ello, a pesar de que la teoría de la propiedad de Hegel se acomoda
a las características de la [PI]{.versalita}, no tiene nada que decir
a favor de la constitución de los derechos de [PI]{.versalita} --parencite(schroeder2004a)--.
Al final, Hegel es una gran ironía dentro de la teoría de la [PI]{.versalita}
--parencite(schroeder2004a)--.

\vskip 1em\noindent La mayoría de los proponentes de esta teoría basan
sus análisis en la *Filosofía del Derecho* de Hegel. Sin embargo, en
la *Fenomenología del Espíritu* y en las *Lecciones de Filosofía de la Historia*
también hay elementos que alejan aún más a Hegel de la intención teórica
de justificar los derechos de la [PI]{.versalita}.

En la *Fenomenología* existe un apartado dedicado a la «obra». Para
Hegel la obra no solo abarca la producción de un objeto artístico o
intelectual, sino en general todo lo producido por el obrar --parencite(hegel2003a)--.
La obra es una realidad que se da en la conciencia, pero no como
conciencia particular, sino como universal --parencite(hegel2003a)--. 
Es una realidad producida por una conciencia individual en cuya
contraposición con otras conciencias deviene su desaparición por medio 
de un juego de fuerzas --parencite(hegel2003a)--. 

La obra si bien tiene orígenes en un sujeto, en su exteriorización es 
asimilada por otros sujetos al grado en que pierde todo rasgo individual 
y pasa a formar parte de las relaciones intersubjetivas: toda obra 
llega a ser parte de la sociedad civil, del Estado, de la historia y, 
por supuesto, del Espíritu. Los derechos de la [PI]{.versalita} podrían
significar la negación del obrar a ser obrar universal así como sería
un indicativo de que la actividad de un sujeto puede ser estrictamente
un acto individual. Como incluso Hughes lo acepta, para Hegel la prioridad
no es el individuo, sino el Estado y el Espíritu, mientras que los
derechos de [PI]{.versalita} se enfocan solo en el individuo 
--parencite(hughes1988a)--.

En las *Lecciones de Filosofía de la Historia*, Hegel habla de los
«grandes hombres». Esta clase de hombres son los que con sus fines
particulares encierran la voluntad del Espíritu --parencite(hegel1971a)--.
Del tranquilo y ordenado transcurso de las cosas, ellos crean un mundo 
que semeja ser su fin y su obra; tienen conciencia de la verdad de su 
tiempo; son el eslabón esencial del mundo: en fin, son una manifestación 
del Espíritu presente --parencite(hegel1971a)--. Esta carácter no fluye
de un entendimiento de las necesidades de sus compatriotas, sino de un
instinto egoísta y una ambición que los hace ser inmorales; realizan lo
universal a través de los intereses particulares de su pasión, es decir,
en la satisfacción de sí mismos dan con una realización del Espíritu 
--parencite(hegel1971a)--. Debido a esta violencia, por lo general este 
tipo de individuos no tienen un destino dichoso: la idea los sacrifica 
ya que una vez alcanzado su fin —que es también el fin del Espíritu—, 
caen --parencite(hegel1971a)--. Son guías para otros hombres, pero también 
serán víctimas de ellos.

En la «teoría de la personalidad» lo que está en juego es la preservación
de aquello que hace a un creador manifestar su personalidad. Tal como
Hegel piensa la historia, cuando esta personalidad es demasiado fuerte,
no hay Estado que garantice su preservación. Y si se va más lejos, esta
clase de sujetos no les interesan este tipo de garantías: no pretenden
obrar según los derechos u obligaciones que otorga un Estado, son ellos
mismos la violación y creación de leyes del nuevo estado de las cosas.
Es decir, los derechos de la [PI]{.versalita} poco pueden hacer cuando 
una invención o una obra artística o literaria sacude la relativa
tranquilidad y orden del mundo presente.

\vskip 1em\noindent Con Hegel se ha pretendido justificar los derechos
de [PI]{.versalita} mediante la supuesta teoría de la personalidad.
No obstante, una característica de semejante teoría es que, de hecho,
poco tiene que ver con Hegel. Así como se ha usado a Hegel para argumentar
a favor del racismo o del colonialismo, el caso de la teoría de la [PI]{.versalita}
es análogo. Los proponentes de tales argumentaciones suponen que su
justificación yace en el pensamiento de Hegel o en su sistema cuando
más bien se han tratado de elementos aislados, sacados de contexto e
incluso malinterpretados para hacer valer sus propios intereses.

La crítica de Schroeder a Hughes es un ejemplo de cómo el pensamiento
hegeliano va en contra de un discurso con raíces hegelianos. En lugar
de una confrontación de Hegel contra el mismo Hegel, en la búsqueda de
la teoría de la [PI]{.versalita} se exhibe qué tan distante pueden estar
los intereses de ciertos intérpretes de Hegel con el mismo sistema de
pensamiento hegeliano.

¿Es Hegel un racista, un colonialista? ¿Está Hegel a favor de los
derechos de la [PI]{.versalita}? ¿Hegel asiente con el «reforzamiento»
a las actuales leyes en torno a la [PI]{.versalita}? Mucho dependerá
de qué tanto se considere que los argumentos en pos de alguna de estas
posturas de manera efectiva corresponden a Hegel y no a los intereses 
de algunos de sus intérpretes…

\vskip 1em\noindent En 2016 las ganancias producidas por la [PI]{.versalita}
de la Unión Europea y de Estados Unidos representaron el 75% de todas
las ganancias producidas por este tipo de economía --parencite(bm2018a)--.
Para ese mismo año, Latinoamérica y el Caribe recibieron un total de 
1,076 miles de millones de dólares, equivalente al 0.32% del ingreso 
global --parencite(bm2018a)--. El caso de China —el país en vías de 
desarrollo qué más empuje ha dado a la economía de la [PI]{.versalita}— 
es muy similiar: 1,161 miles de millones de dólares, igual al 0.34% global
--parencite(bm2018a)--. Por otro lado, para ese mismo año Latinoamérica
y el Caribe tuvieron que pagar 11,558 miles de millones de dólares por 
concepto de [PI]{.versalita} --parencite(bm2018b)--, lo que da una 
balanza negativa de 10,482 miles de millones de dólares. China tuvo
una diferencia aún mayor, al haber pagado 23,979 miles de millones
de dólares por el mismo concepto --parencite(bm2018b)--, quedando en un 
saldo negativo de 22,818 miles de millones de dólares.

Si bien la economía de la [PI]{.versalita} es muy difícil de cuantificar
--parencite(cerlalc2015a)--, es claro que la situación para los países
de tercer mundo o en vías de desarrollo no es optimista. Ante esta
circunstancia, la Organización Mundial de la Propiedad Intelectual ha
retirado a todos los Estados miembros de las Naciones Unidas la necesidad 
de ampliar la producción de [PI]{.versalita}.

Acorde a este organismo, la «innovación impulsa la intensificación del
capital […] y transforma las estructuras económicas» --parencite(ompi2015a)--.
El ecosistema más apropiado para esta innovación es uno donde la [PI]{.versalita}
sea protegida --parencite(ompi2015a)--. Por ende, cada Estado tiene
que implementar políticas que incentiven la producción de [PI]{.versalita},
tal como desde el siglo [XIX]{.versalita} las economías más «avanzadas»
han demostrado cómo este tipo de economía ha acelerado su crecimiento,
principalmente después de la Segunda Guerra Mundial --parencite(ompi2015a)--.

Si a este contexto se suma que la mayoría de la producción de mercancía
se concentra en la «periferia» del primer mundo, principalmente en China 
e India, pero también en Latinoamérica y África. Lugares donde a través 
de procesos de maquila, incluida la «maquila digital» —el *outsourcing* 
de servicios profesionales de perfil tecnológico, como la programación, 
que al final resulta más económico para los centros y supone que da un 
nivel de vida decente al profesionista, aunque sin la posibilidad 
económica o política de poder salir de su propio contexto—, se produce 
una dependencia económica e incluso política hacia los «centros» 
económicos. ¿Qué tanto puede considerarse que esa «economía del conocimiento», 
también llamada economía de la [PI]{.versalita}, se trata de un modelo 
de colonización?

Lo que los derechos de la [PI]{.versalita} buscan es *el control* en
el flujo de la información. Semejante tráfico no se da en un espacio
cartesiano, sino en uno geopolíticamente configurado donde los países
primermundistas o las empresas fundadas en dichos Estados tienden a ser 
los amos de este control del conocimiento. A través del ejercicio de
patentes, diseño industrial o derechos de autor —por poner como ejemplo
los tipos de [PI]{.versalita} más comunes— estas regiones «centrales»
del mundo imponen políticas y modos de interacción económica que provocan
un «intercambio desigual».

Entonces, para concluir con preguntas abiertas, ¿es la [PI]{.versalita}
una forma de colonialismo? ¿Es la [PI]{.versalita} un tipo de expansión 
global de los ideales de la modernidad y del Estado moderno en detrimento
de otros tipos de economías o de organización política? ¿De nueva cuenta
Hegel está siendo empleado para fines distintos a los que pretende su
sistema? ¿Acaso el pensamiento hegeliano viene otra vez a ser usado en 
contra de los intereses de Hegel, por ejemplo, para justificar la 
explotación económica ahora bajo la bandera de la [PI]{.versalita}?
En fin, ¿qué tanto puede desprenderse de la crítica de la [PI]{.versalita}
como crítica a la modernidad europea? ¿La modernidad europea gira en 
torno a las ideas de sujeto y objeto, o más bien a través de la noción 
de sujetos que establecen relaciones de apropiación y expropiación de 
los objetos? ¿Quién es quién y quién es qué —objeto— en esta relación?

---

\begin{flushleft}
Autor: Ramiro Santa Ana Anguiano. \\ Redactado: junio del 2018. \\ Última modificación: junio del 2018. \\ Escrito bajo \href{https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre}{Licencia Editorial Abierta y Libre}. \\ Contenido disponible en \href{http://xxx.cliteratu.re/}{xxx.cliteratu.re}.
\end{flushleft}
